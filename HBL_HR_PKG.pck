create or replace package HBL_HR_PKG is
  procedure create_employee;
  procedure create_emp_address;
  PROCEDURE create_emp_phone;
  procedure create_emp_contact;
  procedure create_med_assesment;
  procedure UPDATE_employee;
  Procedure update_assignments_criteria;
  Procedure update_assignments;
  Procedure emp_upd;
  Procedure emp_pay_method_upd;
  Procedure emp_pay_method_create;
  Procedure emp_costing_create;
  Procedure emp_costing_delete;
  Procedure emp_fnduser_email_upd;
  procedure Update_Position_overlap(p_position_id in number default null,
                                    p_ov          in number default null);
  procedure Update_Position_budgetflag(p_position_id in number default null,
                                    p_ov          in number default null);                                    
  PROCEDURE IBT_EMPLOYEE_API(p_validate                     in boolean default false,
                             p_hire_date                    in date,
                             p_business_group_id            in number,
                             p_last_name                    in varchar2,
                             p_sex                          in varchar2,
                             p_person_type_name             in varchar2 default null,
                             p_nationality_name             in varchar2 default null,
                             p_marital_status               in varchar2 default null,
                             p_title                        in varchar2 default null,
                             p_country_of_birth             in varchar2 default null,
                             p_per_comments                 in varchar2 default null,
                             p_date_employee_data_verified  in date default null,
                             p_date_of_birth                in date default null,
                             p_email_address                in varchar2 default null,
                             p_employee_number              in out nocopy varchar2,
                             p_expense_check_send_to_addres in varchar2 default null,
                             p_first_name                   in varchar2 default null,
                             p_known_as                     in varchar2 default null,
                             p_middle_names                 in varchar2 default null,
                             p_national_identifier          in varchar2 default null,
                             p_previous_last_name           in varchar2 default null,
                             p_registered_disabled_flag     in varchar2 default null,
                             p_vendor_id                    in number default null,
                             p_work_telephone               in varchar2 default null,
                             p_attribute_category           in varchar2 default null,
                             p_attribute1                   in varchar2 default null,
                             p_attribute2                   in varchar2 default null,
                             p_attribute3                   in varchar2 default null,
                             p_attribute4                   in varchar2 default null,
                             p_attribute5                   in varchar2 default null,
                             p_attribute6                   in varchar2 default null,
                             p_attribute7                   in varchar2 default null,
                             p_attribute8                   in varchar2 default null,
                             p_attribute9                   in varchar2 default null,
                             p_attribute10                  in varchar2 default null,
                             p_attribute11                  in varchar2 default null,
                             p_attribute12                  in varchar2 default null,
                             p_attribute13                  in varchar2 default null,
                             p_attribute14                  in varchar2 default null,
                             p_attribute15                  in varchar2 default null,
                             p_attribute16                  in varchar2 default null,
                             p_attribute17                  in varchar2 default null,
                             p_attribute18                  in varchar2 default null,
                             p_attribute19                  in varchar2 default null,
                             p_attribute20                  in varchar2 default null,
                             p_attribute21                  in varchar2 default null,
                             p_attribute22                  in varchar2 default null,
                             p_attribute23                  in varchar2 default null,
                             p_attribute24                  in varchar2 default null,
                             p_attribute25                  in varchar2 default null,
                             p_attribute26                  in varchar2 default null,
                             p_attribute27                  in varchar2 default null,
                             p_attribute28                  in varchar2 default null,
                             p_attribute29                  in varchar2 default null,
                             p_attribute30                  in varchar2 default null,
                             p_per_information_category     in varchar2 default null,
                             p_per_information1             in varchar2 default null,
                             p_per_information2             in varchar2 default null,
                             p_per_information3             in varchar2 default null,
                             p_per_information4             in varchar2 default null,
                             p_per_information5             in varchar2 default null,
                             p_per_information6             in varchar2 default null,
                             p_per_information7             in varchar2 default null,
                             p_per_information8             in varchar2 default null,
                             p_per_information9             in varchar2 default null,
                             p_per_information10            in varchar2 default null,
                             p_per_information11            in varchar2 default null,
                             p_per_information12            in varchar2 default null,
                             p_per_information13            in varchar2 default null,
                             p_per_information14            in varchar2 default null,
                             p_per_information15            in varchar2 default null,
                             p_per_information16            in varchar2 default null,
                             p_per_information17            in varchar2 default null,
                             p_per_information18            in varchar2 default null,
                             p_per_information19            in varchar2 default null,
                             p_per_information20            in varchar2 default null,
                             p_per_information21            in varchar2 default null,
                             p_per_information22            in varchar2 default null,
                             p_per_information23            in varchar2 default null,
                             p_per_information24            in varchar2 default null,
                             p_per_information25            in varchar2 default null,
                             p_per_information26            in varchar2 default null,
                             p_per_information27            in varchar2 default null,
                             p_per_information28            in varchar2 default null,
                             p_per_information29            in varchar2 default null,
                             p_per_information30            in varchar2 default null,
                             p_date_of_death                in date default null,
                             p_background_check_status      in varchar2 default null,
                             p_background_date_check        in date default null,
                             p_blood_type                   in varchar2 default null,
                             p_correspondence_language      in varchar2 default null,
                             p_fast_path_employee           in varchar2 default null,
                             p_fte_capacity                 in number default null,
                             p_honors                       in varchar2 default null,
                             p_internal_location            in varchar2 default null,
                             p_last_medical_test_by         in varchar2 default null,
                             p_last_medical_test_date       in date default null,
                             p_mailstop                     in varchar2 default null,
                             p_office_number                in varchar2 default null,
                             p_on_military_service          in varchar2 default null,
                             p_pre_name_adjunct             in varchar2 default null,
                             p_rehire_recommendation        in varchar2 default null,
                             p_projected_start_date         in date default null,
                             p_resume_exists                in varchar2 default null,
                             p_resume_last_updated          in date default null,
                             p_second_passport_exists       in varchar2 default null,
                             p_student_status               in varchar2 default null,
                             p_work_schedule                in varchar2 default null,
                             p_suffix                       in varchar2 default null,
                             p_benefit_group_id             in number default null,
                             p_receipt_of_death_cert_date   in date default null,
                             p_coord_ben_med_pln_no         in varchar2 default null,
                             p_coord_ben_no_cvg_flag        in varchar2 default 'N',
                             p_coord_ben_med_ext_er         in varchar2 default null,
                             p_coord_ben_med_pl_name        in varchar2 default null,
                             p_coord_ben_med_insr_crr_name  in varchar2 default null,
                             p_coord_ben_med_insr_crr_ident in varchar2 default null,
                             p_coord_ben_med_cvg_strt_dt    in date default null,
                             p_coord_ben_med_cvg_end_dt     in date default null,
                             p_uses_tobacco_flag            in varchar2 default null,
                             p_dpdnt_adoption_date          in date default null,
                             p_dpdnt_vlntry_svce_flag       in varchar2 default 'N',
                             p_original_date_of_hire        in date default null,
                             p_adjusted_svc_date            in date default null,
                             p_town_of_birth                in varchar2 default null,
                             p_region_of_birth              in varchar2 default null,
                             p_global_person_id             in varchar2 default null,
                             p_party_id                     in number default null);

  procedure update_emp_salary(p_empno in varchar2);
end HBL_HR_PKG;
/
CREATE OR REPLACE PACKAGE BODY HBL_HR_PKG IS
  procedure create_employee is
    l_person_id                 per_people_f.person_id%type;
    V_EMPLOYEE_NUMBER           VARCHAR2(200);
    l_assignment_sequence       per_assignments_f.assignment_sequence%type;
    l_assignment_number         per_assignments_f.assignment_number%type;
    l_assignment_id             number;
    l_per_object_version_number number;
    l_asg_object_version_number number;
    l_per_effective_start_date  date;
    l_per_effective_end_date    date;
    l_full_name                 per_people_f.full_name%type;
    l_per_comment_id            number;
    l_name_combination_warning  boolean;
    l_assign_payroll_warning    boolean;
    l_object_version_number     number;
    v_object_version_number     number;
    V_GENDER                    VARCHAR2(100);
    V_BUSINESS_GROUP_ID         NUMBER;
    V_PERSON_TYPE_ID            NUMBER;
    V_MARITAL_STATUS            VARCHAR2(100);
    V_NATIONALITY               VARCHAR2(100);
    V_COUNTRY                   VARCHAR2(100);
    V_BLOOD_GROUP               VARCHAR2(100);
    V_TITLE                     VARCHAR2(100);
    V_STATUS                    VARCHAR2(10);
    V_ERR_MSG                   VARCHAR2(500);
    ln_err_cnt                  number := 0;
    CURSOR EMP_RECORDS IS
      select *
        from XXhBL_EMPLOYEE_STG
       WHERE 1 = 1 --NVL(STATUS, 'N') = 'N'
         and record_id between 344 and 344
       ORDER BY RECORD_ID;
  Begin
    ------------------------------------------------
    for i in EMP_RECORDS LOOP
      --in select * from XXHBL_EMPLOYEE_STG
      -------------------------------------------------
      Begin
        select HAOU.BUSINESS_GROUP_ID
          INTO V_BUSINESS_GROUP_ID
          from HR_ALL_ORGANIZATION_UNITS HAOU
         WHERE 1 = 1 --HAOU.TYPE = 10
           and HAOU.BUSINESS_GROUP_ID = 81
           and HAOU.NAME = I.BUSINESS_GROUP;
        --dbms_output.put_line('Business Group id.' || V_BUSINESS_GROUP_ID);
      Exception
        When NO_DATA_FOUND THEN
          V_STATUS   := 'E';
          ln_err_cnt := ln_err_cnt + 1;
          V_ERR_MSG  := V_ERR_MSG || ' Business Group Not Found.';
          dbms_output.put_line(' Business Group Not Found.');
      END;
      -------------------------------------------------
      Begin
        select DISTINCT lv.lookup_code
          INTO V_GENDER
          from Fnd_Lookup_ValueS LV
         WHERE LV.lookup_type = 'SEX'
           and upper(lv.meaning) = upper(I.GENDER);
      Exception
        When NO_DATA_FOUND THEN
          V_STATUS   := 'E';
          ln_err_cnt := ln_err_cnt + 1;
          V_ERR_MSG  := V_ERR_MSG || ' Gender Not Found.';
          dbms_output.put_line(' Gender Not Found.');
      END;
      -------------------------------------------------
      Begin
        SELECT DISTINCT PPT.PERSON_TYPE_ID
          INTO V_PERSON_TYPE_ID
          FROM per_person_types PPT
         WHERE upper(PPT.USER_PERSON_TYPE) = upper(I.PERSON_TYPE)
           AND PPT.BUSINESS_GROUP_ID =
               (SELECT BG.business_group_id
                  FROM PER_BUSINESS_GROUPS BG
                 WHERE BG.name = I.BUSINESS_GROUP);
        --dbms_output.put_line('Person Type id.' || V_person_type_id);
      Exception
        When NO_DATA_FOUND THEN
          V_STATUS   := 'E';
          ln_err_cnt := ln_err_cnt + 1;
          V_ERR_MSG  := V_ERR_MSG || ' Person Type Not Found.';
          dbms_output.put_line('Person Not Found.');
      END;
      -------------------------------------------------
      If i.marital_status is not null then
        Begin
          SELECT DISTINCT LOOKUP_CODE
            INTO V_MARITAL_STATUS
            FROM FND_LOOKUP_VALUES FLV
           WHERE FLV.LOOKUP_TYPE LIKE 'MAR_STATUS'
             AND UPPER(FLV.MEANING) = UPPER(I.MARITAL_STATUS);
        Exception
          When NO_DATA_FOUND THEN
            V_STATUS   := 'E';
            ln_err_cnt := ln_err_cnt + 1;
            V_ERR_MSG  := V_ERR_MSG || ' Marital Status Not Found.';
            dbms_output.put_line('Marital Status Not Found.');
        END;
      End if;
      -------------------------------------------------
      if i.nationality is not null then
        Begin
          SELECT DISTINCT LV.LOOKUP_CODE
            INTO V_NATIONALITY
            FROM FND_LOOKUP_VALUES LV
           WHERE LV.LOOKUP_TYPE = 'NATIONALITY'
             AND UPPER(LV.MEANING) = UPPER(I.NATIONALITY);
        Exception
          When NO_DATA_FOUND THEN
            V_STATUS   := 'E';
            ln_err_cnt := ln_err_cnt + 1;
            V_ERR_MSG  := V_ERR_MSG || ' Nationality Not Found.';
            dbms_output.put_line('Nationality Not Found.');
        END;
      end if;
      -------------------------------------------------
      If i.country is not null then
        Begin
          SELECT FTV.TERRITORY_CODE
            INTO V_COUNTRY
            FROM FND_TERRITORIES_VL FTV
           WHERE UPPER(FTV.TERRITORY_SHORT_NAME) = UPPER(I.COUNTRY);
        Exception
          When NO_DATA_FOUND THEN
            V_STATUS   := 'E';
            ln_err_cnt := ln_err_cnt + 1;
            V_ERR_MSG  := V_ERR_MSG || ' Country Not Found.';
            dbms_output.put_line('Country Not Found.');
        END;
      End if;
      -------------------------------------------------
      If i.blood_group != 'ZZZ' then
        if i.blood_group is not null then
          Begin
            SELECT DISTINCT LV.LOOKUP_CODE
              INTO V_BLOOD_GROUP
              FROM FND_LOOKUP_VALUES LV
             WHERE LV.LOOKUP_TYPE = 'BLOOD_TYPE'
               AND UPPER(LV.LOOKUP_CODE) = UPPER(I.BLOOD_GROUP);
          Exception
            When NO_DATA_FOUND THEN
              V_STATUS   := 'E';
              ln_err_cnt := ln_err_cnt + 1;
              V_ERR_MSG  := V_ERR_MSG || ' Blood Group Not Found.';
              dbms_output.put_line('Blood Group Not Found.');
          END;
        end if;
      End if;
      -------------------------------------------------
      Begin
        SELECT DISTINCT LV.LOOKUP_CODE
          INTO V_TITLE
          FROM FND_LOOKUP_VALUES LV
         WHERE LV.LOOKUP_TYPE = 'TITLE'
           AND UPPER(LV.MEANING) = UPPER(I.TITLE);
      Exception
        When NO_DATA_FOUND THEN
          V_STATUS   := 'E';
          ln_err_cnt := ln_err_cnt + 1;
          V_ERR_MSG  := V_ERR_MSG || 'Title Not Found.';
          dbms_output.put_line('Title Not Found.');
      END;
      -------------------------------------------------
      if ln_err_cnt = 0 then
        begin
          -- Call the procedure
          hr_employee_api.create_employee(p_validate                    => False,
                                          p_hire_date                   => I.Date_Of_Joining, --sysdate--cc_emp.date_of_join
                                          p_business_group_id           => V_BUSINESS_GROUP_ID, --fnd_profile.VALUE('PER_BUSINESS_GROUP_ID'),
                                          p_last_name                   => initcap(I.last_name),
                                          p_sex                         => V_GENDER,
                                          p_person_type_id              => V_PERSON_TYPE_ID, -- for employee
                                          p_per_comments                => NULL,
                                          p_date_employee_data_verified => null,
                                          p_date_of_birth               => I.Brith_Date,
                                          p_email_address               => ltrim(I.Email_Address),
                                          p_employee_number             => I.Employee_Number,
                                          p_first_name                  => initcap(I.First_Name),
                                          P_MIDDLE_NAMES                => initcap(I.Middle_Name),
                                          p_known_as                    => NULL,
                                          p_marital_status              => V_MARITAL_STATUS,
                                          p_nationality                 => V_NATIONALITY, --/*cc_emp.nationality--*/'PQH_PK'
                                          p_national_identifier         => I.CNIC,
                                          p_registered_disabled_flag    => 'N',
                                          p_title                       => V_TITLE,
                                          p_background_check_status     => 'N',
                                          p_on_military_service         => 'N',
                                          p_resume_exists               => 'N',
                                          p_town_of_birth               => I.TOWN_OF_BIRTH,
                                          --p_blood_type               => V_BLOOD_GROUP, -- 'A+'
                                          --                                          p_date_of_death            => i.date_of_death,
                                          p_country_of_birth => V_COUNTRY, --'PK'
                                          -- p_attribute_category       => 'Personal Extra Information',
                                          p_attribute1  => i.religion, --HBL_Religion_VS
                                          p_attribute2  => i.ntn_no,
                                          p_attribute3  => i.mother_tongue, --HBL_MotherTounge_VS
                                          p_attribute4  => null, --PASSPORT
                                          p_attribute5  => null, -- pssport expire dt
                                          p_attribute6  => null, -- nic exprirey dt
                                          p_attribute7  => null,
                                          p_attribute8  => null,
                                          p_attribute9  => null,
                                          p_attribute10 => null,
                                          p_attribute11 => null,
                                          ------------------------------------------------------------
                                          /* ,P_ATTRIBUTE_CATEGORY            => NULL
                                                                                                                                                                                                                                                                                                                                                                        ,p_attribute1                    => I.OLD_EMPLOYEE_NUMBER
                                                                                                                                                                                                                                                                                                                                                                        ,p_attribute2                    => I.OLD_NIC
                                                                                                                                                                                                                                                                                                                                                                        ,p_attribute3                    => I.NTN
                                                                                                                                                                                                                                                                                                                                                                        ,p_attribute4                    => I.RELIGION
                                                                                                                                                                                                                                                                                                                                                                        --,p_attribute5                    => V_BLOOD_GROUP
                                                                                                                                                                                                                                                                                                                                                                        ,p_attribute6                    => I.DOMICILE*/
                                          ------------------------------------------------------------
                                          p_person_id                 => l_person_id,
                                          p_assignment_id             => l_assignment_id,
                                          p_per_object_version_number => l_per_object_version_number,
                                          p_asg_object_version_number => l_asg_object_version_number,
                                          p_per_effective_start_date  => l_per_effective_start_date,
                                          p_per_effective_end_date    => l_per_effective_end_date,
                                          p_full_name                 => l_full_name --I.FULL_NAME
                                         ,
                                          p_per_comment_id            => l_per_comment_id,
                                          p_assignment_sequence       => l_assignment_sequence,
                                          p_assignment_number         => l_assignment_number,
                                          p_name_combination_warning  => l_name_combination_warning,
                                          p_assign_payroll_warning    => l_assign_payroll_warning);
          --dbms_output.put_line('After API' || l_person_id);
          -- DBMS_OUTPUT.put_line('Employee   - '||l_full_name||' Having Person ID - '/*l_person_id*/||' has been created Successfully.');
          IF l_person_id = 0 OR l_person_id IS NULL THEN
            UPDATE XXHBL_EMPLOYEE_STG
               SET status = 'E', error_msg = 'Error arises in Employee API'
             WHERE record_id = i.record_id;
            dbms_output.put_line('error : ');
          ELSE
            UPDATE XXHBL_EMPLOYEE_STG
               SET status = 'S', person_id = l_person_id, error_msg = ' '
             WHERE record_id = i.record_id;
            --dbms_output.put_line('Employee Created Successfully.');
          END IF;
        EXCEPTION
          WHEN OTHERS THEN
            V_ERR_MSG := V_ERR_MSG || ' ' || SQLCODE || '.' || SQLERRM;
            UPDATE XXHBL_EMPLOYEE_STG
               SET status = 'E', error_msg = V_ERR_MSG
             WHERE record_id = i.record_id;
            dbms_output.put_line('Exception in API');
            v_err_msg  := null;
            ln_err_cnt := 0;
            V_STATUS   := null;
        END;
      Else
        UPDATE XXHBL_EMPLOYEE_STG
           SET status = 'E', error_msg = V_ERR_MSG
         WHERE record_id = i.record_id;
      End if;
      v_err_msg  := null;
      ln_err_cnt := 0;
      v_status   := null;
    END LOOP;
    -- COMMIT;
  exception
    when others then
      dbms_output.put_line('error : ' || sqlerrm);
  end create_employee;
  ------------------------------------------------------------------------------------
  /*--------------------------Create Employee Address-------------------------------*/
  ------------------------------------------------------------------------------------
  procedure create_emp_address is
    ln_address_id            per_addresses.address_id%type;
    ln_person_id             per_all_people_f.person_id%type;
    ln_object_version_number NUMBER;
    V_BUSINESS_GROUP_ID      NUMBER;
    V_ADDRESS_TYPE           VARCHAR2(30);
    V_COUNTRY                VARCHAR2(100);
    V_STATUS                 VARCHAR2(10);
    V_ERR_MSG                VARCHAR2(500);
    ln_err_cnt               number := 0;
    ld_start_date            date;
    V_DOMICILE               varchar2(40);
    CURSOR emp_address_cur IS
    /*s.record_id,
                 s.business_group,
                 s.employee_number,
                 s.address_type,
                 decode(s.primary_flag, 'Yes', 'Y', 'No', 'N') primary_flag,
                 s.address_line1,
                 s.address_line2,
                 s.address_line3,
                 ltrim(s.city, ' ') city,
                 s.tehsil,
                 s.district,
                 s.region,
                 s.province,
                 s.country,
                 s.postal_code,
                 s.phone_no1,
                 s.phone_no2,
                 s.mobile_no1,
                 s.mobile_no2,
                 s.fax,
                 s.email,
                 s.date_from,
                 s.date_to
                 */
      select * from XXHBL_EMP_ADDRESS_STG s WHERE NVL(STATUS, 'N') = 'N';
    --and  s.record_id between 2 and 2;
    --and s.record_id = 15133 --between 10001 and 15000
    /*and  s.employee_number not in (select e.employee_number from Xxubl_Emp_Address_Stg e
          where e.primary_flag = 'Yes'
          having count(*) > 1
          group by e.employee_number,e.primary_flag
    )*/
    --and s.employee_number='284592'
    --and s.Primary_Flag ='No'
    --       order by s.employee_number, s.date_from;
  Begin
    ------------------------------------------------
    for i in emp_address_cur LOOP
      --in select * from XXUBL_EMPLOYEE_STG
      -------------------------------------------------
      Begin
        select DISTINCT HAOU.BUSINESS_GROUP_ID
          INTO V_BUSINESS_GROUP_ID
          from HR_ALL_ORGANIZATION_UNITS HAOU
         WHERE --HAOU.TYPE = 10
         HAOU.BUSINESS_GROUP_ID = 81
         and HAOU.NAME = I.BUSINESS_GROUP;
        --dbms_output.put_line('Business Group id.' || V_BUSINESS_GROUP_ID);
      Exception
        When NO_DATA_FOUND THEN
          V_STATUS   := 'E';
          ln_err_cnt := ln_err_cnt + 1;
          V_ERR_MSG  := V_ERR_MSG || ' Business Group Not Found.';
          dbms_output.put_line(' Business Group Not Found.');
      END;
      ------------------------------------------------------
      If i.Address_Type is not null then
        Begin
          SELECT DISTINCT LV.LOOKUP_CODE
            INTO V_ADDRESS_TYPE
            FROM FND_LOOKUP_VALUES LV
           WHERE LV.LOOKUP_TYPE = 'ADDRESS_TYPE'
             AND UPPER(LV.Meaning) = UPPER(i.address_type);
        Exception
          When NO_DATA_FOUND THEN
            V_STATUS   := 'E';
            ln_err_cnt := ln_err_cnt + 1;
            V_ERR_MSG  := V_ERR_MSG || 'Address Type Not Found.';
            dbms_output.put_line('Address Type Not Found.');
        END;
      End if;
      -------------------------------------------------
      Begin
        SELECT P.PERSON_ID, p.start_date
          INTO ln_person_id, ld_start_date
          FROM PER_ALL_PEOPLE_F P
         WHERE TRUNC(SYSDATE) BETWEEN P.EFFECTIVE_START_DATE AND
               P.EFFECTIVE_END_DATE
           and p.business_group_id = V_BUSINESS_GROUP_ID
           AND P.EMPLOYEE_NUMBER = I.EMPLOYEE_NUMBER;
      Exception
        When NO_DATA_FOUND THEN
          V_STATUS   := 'E';
          ln_err_cnt := ln_err_cnt + 1;
          V_ERR_MSG  := V_ERR_MSG || 'Employee Not Found.';
          dbms_output.put_line('Employee Not Found.');
      END;
      -------------------------------------------------
      If i.country is not null then
        Begin
          SELECT FTV.TERRITORY_CODE
            INTO V_COUNTRY
            FROM FND_TERRITORIES_VL FTV
           WHERE FTV.TERRITORY_SHORT_NAME = ltrim(initcap(i.country));
        Exception
          When NO_DATA_FOUND THEN
            V_STATUS   := 'E';
            ln_err_cnt := ln_err_cnt + 1;
            V_ERR_MSG  := V_ERR_MSG || ' Country Not Found.';
            dbms_output.put_line('Country Not Found.');
        END;
      End if;
      -------------------------------------------------
      -------------------------------------------------
      If I.DOMICILE is not null then
        Begin
        
          SELECT G.flex_value
            INTO V_DOMICILE
            FROM FND_FLEX_VALUES G
           WHERE G.FLEX_VALUE_SET_ID = '1016218'
             AND UPPER(G.FLEX_VALUE) = UPPER(I.DOMICILE);
        
        Exception
          When NO_DATA_FOUND THEN
            V_STATUS   := 'E';
            ln_err_cnt := ln_err_cnt + 1;
            V_ERR_MSG  := V_ERR_MSG || ' Domicile not found';
            dbms_output.put_line('Domicile not found.');
        END;
      End if;
      -------------------------------------------------
      if ln_err_cnt = 0 then
        begin
          -- Call the procedure
        
          hr_person_address_api.create_person_address(p_validate       => false,
                                                      p_effective_date => i.date_from,
                                                      p_person_id      => ln_person_id,
                                                      p_style          => 'PK',
                                                      p_primary_flag   => i.primary_flag,
                                                      p_address_type   => V_ADDRESS_TYPE,
                                                      p_address_line1  => i.address_line3, --i.address_line1,
                                                      -- p_address_line2        => i.address_line2,
                                                      -- p_address_line3         => i.address_line3,
                                                      p_town_or_city      => initcap(Trim(i.city)),
                                                      p_add_information13 => i.domicile, --i.tehsil, --domicile
                                                      p_add_information14 => null, --email
                                                      p_region_1          => initcap(trim(i.province)),
                                                      p_add_information15 => null,
                                                      p_country           => V_COUNTRY,
                                                      p_postal_code       => i.postal_code,
                                                      /*                                                      p_telephone_number_1    => null,
                                                                                                            p_telephone_number_2    => null,
                                                                                                            p_telephone_number_3    => null,
                                                                                                            p_add_information18     => null,
                                                                                                            p_add_information16     => null,*/
                                                      p_date_from             => i.date_from, --sysdate,
                                                      p_date_to               => i.date_to,
                                                      p_address_id            => ln_address_id,
                                                      p_object_version_number => ln_object_version_number);
          --dbms_output.put_line('After API' || ln_address_id);
          -- DBMS_OUTPUT.put_line('Employee   - '||l_full_name||' Having Person ID - '/*l_person_id*/||' has been created Successfully.');
          IF ln_address_id = 0 OR ln_address_id IS NULL THEN
            UPDATE XXHBL_EMP_ADDRESS_STG
               SET status = 'E', error_msg = 'Error arises in Address API'
             WHERE record_id = i.record_id;
            --dbms_output.put_line('error : ');
          ELSE
            UPDATE XXHBL_EMP_ADDRESS_STG
               SET status     = 'S',
                   error_msg  = ' ',
                   address_id = ln_address_id
             WHERE record_id = i.record_id;
            --dbms_output.put_line('Employee Address Created Successfully.');
          END IF;
        EXCEPTION
          WHEN OTHERS THEN
            V_ERR_MSG := V_ERR_MSG || ' ' || SQLCODE || '.' || SQLERRM;
            UPDATE XXHBL_EMP_ADDRESS_STG
               SET status = 'E', error_msg = V_ERR_MSG
             WHERE record_id = i.record_id;
            dbms_output.put_line('Exception in API');
            v_err_msg  := null;
            ln_err_cnt := 0;
            V_STATUS   := null;
        END;
      Else
        UPDATE XXHBL_EMP_ADDRESS_STG
           SET status = 'E', error_msg = V_ERR_MSG
         WHERE record_id = i.record_id;
      End if;
      v_err_msg  := null;
      ln_err_cnt := 0;
      v_status   := null;
    END LOOP;
    -- COMMIT;
  exception
    when others then
      dbms_output.put_line('error : ' || sqlerrm);
  end create_emp_address;
  ------------------------------------------------------------------------------------
  /*--------------------------Create Employee Phone-------------------------------*/
  ------------------------------------------------------------------------------------
  PROCEDURE create_emp_phone IS
    CURSOR cur_emp_phone IS
      SELECT *
        FROM XXHBL_EMP_PHONE_STG
       WHERE NVL(status, 'N') = 'N'
      --AND RECORD_ID between 1 and 1
       order by record_id;
    --
    -- Declaration of package private cursors
    --
    /* Get Business Group id */
    CURSOR cur_business_group_id(p_business_group_name varchar2) IS
      SELECT BUSINESS_GROUP_ID
        FROM PER_BUSINESS_GROUPS
       WHERE UPPER(NAME) = UPPER(p_business_group_name);
    /* Get person id */
    CURSOR cur_person_id(p_emp_no varchar2, p_business_group_id number) IS
      SELECT PERSON_ID, START_DATE
        FROM PER_ALL_PEOPLE_F
       WHERE EMPLOYEE_NUMBER = p_emp_no
         AND BUSINESS_GROUP_ID = p_business_group_id
         AND TRUNC(SYSDATE) BETWEEN EFFECTIVE_START_DATE AND
             EFFECTIVE_END_DATE
         AND CURRENT_EMPLOYEE_FLAG = 'Y';
    /* Get Phone Type */
    CURSOR cur_phone_type(p_type varchar2) IS
      SELECT LOOKUP_CODE
        FROM HR_LOOKUPS
       WHERE UPPER(MEANING) = UPPER(trim(p_type))
         and lookup_type = 'PHONE_TYPE';
    ln_phone_id              PER_PHONES.PHONE_ID%TYPE;
    ln_object_version_number PER_PHONES.OBJECT_VERSION_NUMBER%TYPE;
    lc_phone_type            varchar2(30);
    ln_person_id             number;
    ln_business_group_id     number;
    lc_err_flag              char;
    ln_err_cnt               number := 0;
    lc_error_message         varchar2(500);
    ld_start_date            date;
  BEGIN
    FOR i IN cur_emp_phone LOOP
      --dbms_output.put_line('Initial Phone Type' || i.phone_type);
      lc_err_flag      := 'N';
      lc_error_message := NULL;
      /* Checking Business Group id */
      OPEN cur_business_group_id(i.business_group);
      FETCH cur_business_group_id
        INTO ln_business_group_id;
      IF (cur_business_group_id%NOTFOUND) THEN
        lc_err_flag      := 'E';
        ln_err_cnt       := ln_err_cnt + 1;
        lc_error_message := lc_error_message ||
                            ' Employee Business Group Not Found.';
      END IF;
      CLOSE cur_business_group_id;
      /* Checking Person id */
      OPEN cur_person_id(i.employee_number, ln_business_group_id);
      FETCH cur_person_id
        INTO ln_person_id, ld_start_date;
      IF (cur_person_id%NOTFOUND) THEN
        lc_err_flag      := 'E';
        ln_err_cnt       := ln_err_cnt + 1;
        lc_error_message := lc_error_message || ' Employee Not Found.';
      END IF;
      CLOSE cur_person_id;
      /* Checking Phone Type */
      OPEN cur_phone_type(i.Phone_Type);
      FETCH cur_phone_type
        INTO lc_phone_type;
      --dbms_output.put_line('Phone Type' || lc_phone_type);
      IF (cur_phone_type%NOTFOUND) THEN
        lc_err_flag      := 'E';
        ln_err_cnt       := ln_err_cnt + 1;
        lc_error_message := lc_error_message || ' Phone Type Not Found.';
        dbms_output.put_line('PT' || lc_phone_type);
      END IF;
      CLOSE cur_phone_type;
      if ln_err_cnt = 0 then
        Begin
          -- Create or Update Employee Phone Detail
          -- -----------------------------------------------------------
          hr_phone_api.create_phone( -- Input data elements
                                    -- -----------------------------
                                    p_date_from      => TO_DATE(i.start_date,
                                                                'DD-MON-RRRR'),
                                    p_phone_type     => lc_phone_type,
                                    p_phone_number   => i.phone_number,
                                    p_parent_id      => ln_person_id,
                                    p_parent_table   => 'PER_ALL_PEOPLE_F',
                                    p_effective_date => TO_DATE(i.start_date,
                                                                'DD-MON-RRRR'),
                                    -- Output data elements
                                    -- --------------------------------
                                    p_phone_id              => ln_phone_id,
                                    p_object_version_number => ln_object_version_number);
          --dbms_output.put_line('Phone id'||'-'||ln_phone_id);
        Exception
          when others then
            lc_err_flag      := 'E';
            ln_err_cnt       := ln_err_cnt + 1;
            lc_error_message := lc_error_message || ' Phone Api Error: ' ||
                                SQLERRM;
        End;
        IF ln_err_cnt >= 1 THEN
          UPDATE XXHBL_EMP_PHONE_STG
             SET status = lc_err_flag, error_msg = lc_error_message
           WHERE record_id = i.record_id;
        ELSE
          UPDATE XXHBL_EMP_PHONE_STG
             SET status = 'S', phone_id = ln_phone_id, error_msg = ''
           WHERE record_id = i.record_id;
        END IF;
      else
        UPDATE XXHBL_EMP_PHONE_STG
           SET status = lc_err_flag, error_msg = lc_error_message
         WHERE record_id = i.record_id;
      end if;
      lc_err_flag      := null;
      ln_err_cnt       := 0;
      lc_error_message := null;
    End loop;
    --COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      dbms_output.put_line(SQLERRM);
  END create_emp_phone;
  procedure create_emp_contact is
    ln_person_id                 per_contact_relationships.person_id%type;
    ln_business_group_id         per_contact_relationships.business_group_id%type;
    ln_ctr_object_version_number per_contact_relationships.object_version_number%type;
    ln_per_object_version_number per_contact_relationships.object_version_number%type;
    ln_ctr_relationship_id       per_contact_relationships.contact_relationship_id%type;
    ln_ctr_person_id             per_contact_relationships.contact_person_id%type;
    ld_per_effective_start_date  date;
    ld_per_effective_end_date    date;
    lc_full_name                 per_people_f.full_name%type;
    lc_contact_type              per_contact_relationships.contact_type%type;
    ln_per_comment_id            PER_ALL_PEOPLE_F.COMMENT_ID%TYPE;
    lb_name_combination_warning  boolean;
    lb_orig_hire_warning         boolean;
    lc_gender                    VARCHAR2(100);
    lc_err_flag                  char;
    ln_err_cnt                   number := 0;
    lc_error_message             varchar2(500);
    lc_town_of_birth             varchar2(500);
    -- Phone API Variables
    ln_phone_id                 PER_PHONES.PHONE_ID%TYPE;
    ln_ph_object_version_number PER_PHONES.OBJECT_VERSION_NUMBER%TYPE;
    lc_phone_type               varchar2(30);
    -- Address API Variables
    ln_address_id                 per_addresses.address_id%type;
    ln_addr_object_version_number NUMBER;
    V_PERSON_TYPE_ID              NUMBER;
    V_MARITAL_STATUS              VARCHAR2(100);
    V_NATIONALITY                 VARCHAR2(100);
    V_COUNTRY                     VARCHAR2(100);
    V_BLOOD_GROUP                 VARCHAR2(100);
    V_TITLE                       VARCHAR2(100);
    ld_start_date                 date;
    lc_primary_flag               char(1);
    -- V_STATUS                    VARCHAR2(10);
    --  V_ERR_MSG                   VARCHAR2(500);
    CURSOR cur_emp_contact IS
      select *
        from XXHBL_EMP_CONTACT_STG ec
       WHERE NVL(STATUS, 'N') = 'N'
         and exists
       (select ''
                from xxhbl_employee_stg e
               where e.employee_number = ec.employee_number)
      -- AND record_id between 2 and 2
      --and employee_number='541877'
       order by record_id;
    /*select *
      from xxubl_emp_contact_stg cs
     where cs.employee_number || cs.first_name || cs.middle_name ||
           cs.last_name in
           (select name
              from (select c.employee_number,
                           c.full_name,
                           c.relation_type,
                           c.employee_number || c.first_name || c.middle_name ||
                           c.last_name name,
                           count(*)
                      from xxubl_emp_contact_stg c
                    --where c.first_name||c.last_name='MohammadKhan'
                    having count(*) > 1
                     group by c.employee_number,
                              c.full_name,
                              c.relation_type,
                              c.employee_number || c.first_name || c.middle_name ||
                              c.last_name))
    and NVL(cs.STATUS, 'N') = 'N'
    --and cs.record_id between 40001 and 46000
    and cs.employee_number >= 600305
      order by cs.employee_number,cs.start_date;*/
    --
    -- Declaration of package private cursors
    --
    /* Get Business Group id */
    CURSOR cur_business_group_id(p_business_group_name varchar2) IS
      SELECT BUSINESS_GROUP_ID
        FROM PER_BUSINESS_GROUPS
       WHERE UPPER(NAME) = UPPER(p_business_group_name);
    /* Get person id */
    CURSOR cur_person_id(p_emp_no varchar2, p_business_group_id number) IS
      SELECT PERSON_ID, start_date
        FROM PER_ALL_PEOPLE_F
       WHERE EMPLOYEE_NUMBER = p_emp_no
         AND BUSINESS_GROUP_ID = p_business_group_id
         AND TRUNC(SYSDATE) BETWEEN EFFECTIVE_START_DATE AND
             EFFECTIVE_END_DATE
         AND CURRENT_EMPLOYEE_FLAG = 'Y';
    /* Get Contact Type */
    CURSOR cur_contact_type(p_type varchar2) IS
      select lookup_code
        from hr_lookups
       where lookup_type = 'CONTACT'
         and upper(meaning) = upper(p_type);
    /* Get Phone Type */
    /* CURSOR cur_phone_type(p_ph_type varchar2) IS
    SELECT LOOKUP_CODE
      FROM HR_LOOKUPS
     WHERE UPPER(MEANING) = UPPER(p_ph_type)
       and lookup_type = 'PHONE_TYPE';*/
  Begin
    ------------------------------------------------
    for i in cur_emp_contact LOOP
      -------------------------------------------------
      lc_err_flag      := 'N';
      lc_error_message := NULL;
      /* Checking Business Group id */
      OPEN cur_business_group_id(i.business_group);
      FETCH cur_business_group_id
        INTO ln_business_group_id;
      IF (cur_business_group_id%NOTFOUND) THEN
        lc_err_flag      := 'E';
        ln_err_cnt       := ln_err_cnt + 1;
        lc_error_message := lc_error_message ||
                            ' Employee Business Group Not Found.';
      END IF;
      CLOSE cur_business_group_id;
      /* Checking Person id */
      OPEN cur_person_id(i.employee_number, ln_business_group_id);
      FETCH cur_person_id
        INTO ln_person_id, ld_start_date;
      IF (cur_person_id%NOTFOUND) THEN
        lc_err_flag      := 'E';
        ln_err_cnt       := ln_err_cnt + 1;
        lc_error_message := lc_error_message || ' Employee Not Found.';
      END IF;
      CLOSE cur_person_id;
      /* Checking Contact_type */
      OPEN cur_contact_type(i.relation_type);
      FETCH cur_contact_type
        INTO lc_contact_type;
      IF (cur_contact_type%NOTFOUND) THEN
        lc_err_flag      := 'E';
        ln_err_cnt       := ln_err_cnt + 1;
        lc_error_message := lc_error_message || ' Contact Type Not Found.';
      END IF;
      CLOSE cur_contact_type;
      /* Checking Phone Type */
      /*if i.phone_type is not null then
        OPEN cur_phone_type(i.Phone_Type);
          FETCH cur_phone_type
          INTO lc_phone_type;
          --dbms_output.put_line('Phone Type' || lc_phone_type);
            IF (cur_phone_type%NOTFOUND) THEN
                lc_err_flag      := 'E';
                ln_err_cnt       := ln_err_cnt + 1;
                lc_error_message := lc_error_message || ' Phone Type Not Found.';
                --dbms_output.put_line('PT' || lc_phone_type);
            END IF;
         CLOSE cur_phone_type;
      End if;*/
      -------------------------------------------------
      If i.gender is not null then
        Begin
          select DISTINCT lv.lookup_code
            INTO lc_gender
            from Fnd_Lookup_ValueS LV
           WHERE LV.lookup_type = 'SEX'
             and upper(lv.meaning) = upper(I.GENDER);
        Exception
          When NO_DATA_FOUND THEN
            lc_err_flag      := 'E';
            ln_err_cnt       := ln_err_cnt + 1;
            lc_error_message := lc_error_message || ' Gender Not Found.';
            --dbms_output.put_line(' Gender Not Found.');
        END;
      End if;
      -------------------------------------------------
      If i.marital_status is not null then
        Begin
          SELECT DISTINCT LOOKUP_CODE
            INTO V_MARITAL_STATUS
            FROM FND_LOOKUP_VALUES FLV
           WHERE FLV.LOOKUP_TYPE LIKE 'MARITAL_STATUS%'
             AND UPPER(FLV.MEANING) = UPPER(I.MARITAL_STATUS);
        Exception
          When NO_DATA_FOUND THEN
            lc_err_flag      := 'E';
            ln_err_cnt       := ln_err_cnt + 1;
            lc_error_message := lc_error_message ||
                                ' Marital Status Not Found.';
            --dbms_output.put_line('Marital Status Not Found.');
        END;
      End if;
      -------------------------------------------------
      -------------------------------------------------
      IF i.country is not null then
        Begin
          SELECT FTV.TERRITORY_CODE
            INTO V_COUNTRY
            FROM FND_TERRITORIES_VL FTV
           WHERE FTV.TERRITORY_SHORT_NAME = ltrim(initcap(i.country));
          /*SELECT DISTINCT LV.LOOKUP_CODE
           INTO V_COUNTRY
           FROM FND_LOOKUP_VALUES LV
          WHERE LV.LOOKUP_TYPE = 'PER_US_COUNTRY_CODE'
            AND UPPER(LV.MEANING) = UPPER(I.COUNTRY);*/
        Exception
          When NO_DATA_FOUND THEN
            lc_err_flag      := 'E';
            ln_err_cnt       := ln_err_cnt + 1;
            lc_error_message := lc_error_message || ' Country Not Found.';
            --dbms_output.put_line('Country Not Found.');
        END;
      End If;
      -------------------------------------------------
      if i.title is not null then
        Begin
          SELECT DISTINCT LV.LOOKUP_CODE
            INTO V_TITLE
            FROM FND_LOOKUP_VALUES LV
           WHERE LV.LOOKUP_TYPE = 'TITLE'
             AND UPPER(LV.MEANING) = UPPER(I.TITLE);
        Exception
          When NO_DATA_FOUND THEN
            lc_err_flag      := 'E';
            ln_err_cnt       := ln_err_cnt + 1;
            lc_error_message := lc_error_message || 'Title Not Found.';
            --dbms_output.put_line('Title Not Found.');
        END;
      End if;
      /*Begin
          SELECT a.city
            INTO lc_town_of_birth
          FROM Xxubl_Emp_Address_Stg A
          WHERE a.employee_number = i.employee_number;
      Exception
        When NO_DATA_FOUND THEN
          lc_err_flag      := 'E';
          ln_err_cnt       := ln_err_cnt + 1;
          lc_error_message := lc_error_message || 'Town of Birth Not Found';
      END;*/
      /*if i.relation_type = 'Wife' or i.relation_type ='Husband' then
         lc_primary_flag :='Y';
      else
         lc_primary_flag :='N';
      end if;   */
      /*if i.addr_date_from is null then
        lc_err_flag      := 'E';
        ln_err_cnt       := ln_err_cnt + 1;
        lc_error_message := lc_error_message ||
                            'Address Start date is required';
      end if;
      if i.postal_code is null then
        lc_err_flag      := 'E';
        ln_err_cnt       := ln_err_cnt + 1;
        lc_error_message := lc_error_message ||
                            'Address Postal Code is required';
      end if;*/
      -------------------------------------------------
      if ln_err_cnt = 0 then
        begin
          -- Call the Employee Contact API
          --dbms_output.put_line('In Contact API');
          hr_contact_rel_api.create_contact(p_validate          => FALSE,
                                            p_start_date        => i.start_date, --ld_start_date,
                                            p_business_group_id => ln_business_group_id,
                                            p_person_id         => ln_person_id,
                                            p_contact_type      => lc_contact_type,
                                            --                                            p_primary_contact_flag      => 'Y',
                                            p_date_start       => i.start_date,
                                            p_personal_flag    => 'Y',
                                            p_cont_attribute1  => null,
                                            p_cont_attribute2  => null,
                                            p_cont_attribute3  => null,
                                            p_cont_attribute4  => null,
                                            p_cont_attribute5  => null,
                                            p_cont_attribute6  => null,
                                            p_cont_attribute7  => null,
                                            p_cont_attribute8  => null,
                                            p_cont_attribute9  => null,
                                            p_cont_attribute10 => null,
                                            p_cont_attribute11 => null,
                                            p_cont_attribute12 => null,
                                            p_cont_attribute13 => null,
                                            p_cont_attribute14 => null,
                                            p_cont_attribute15 => null,
                                            p_cont_attribute16 => null,
                                            p_cont_attribute17 => null,
                                            p_cont_attribute18 => null,
                                            /*                                            p_dependent_flag            => nvl(i.dependent_flag,
                                                                                                                           'N'),
                                                                                        p_beneficiary_flag          => nvl(i.beneficiary_flag,
                                                                                                                           'N'),
                                                                                        p_cont_information10        => i.verified_status,
                                                                                        p_cont_information11        => i.verified_by,
                                                                                        p_cont_information12        => i.verified_date,
                                            */
                                            p_last_name           => i.last_name,
                                            p_sex                 => lc_gender,
                                            p_person_type_id      => null,
                                            p_date_of_birth       => i.brith_date,
                                            p_first_name          => i.first_name,
                                            p_marital_status      => V_MARITAL_STATUS,
                                            p_middle_names        => i.middle_name,
                                            p_national_identifier => i.cnic,
                                            p_title               => V_TITLE,
                                            /*                                            p_attribute2                => null,
                                                                                        p_attribute3                => i.ntn,
                                                                                        p_attribute4                => i.religion,
                                                                                        p_attribute5                => i.spi_eligibility,
                                                                                        p_attribute7                => i.domicile,
                                                                                        p_attribute9                => i.ailment,
                                                                                        p_attribute10               => i.symbol_customer_no,
                                            */
                                            p_contact_relationship_id   => ln_ctr_relationship_id,
                                            p_ctr_object_version_number => ln_ctr_object_version_number,
                                            p_per_person_id             => ln_ctr_person_id,
                                            p_per_object_version_number => ln_per_object_version_number,
                                            p_per_effective_start_date  => ld_per_effective_start_date,
                                            p_per_effective_end_date    => ld_per_effective_end_date,
                                            p_full_name                 => lc_full_name,
                                            p_per_comment_id            => ln_per_comment_id,
                                            p_name_combination_warning  => lb_name_combination_warning,
                                            p_orig_hire_warning         => lb_orig_hire_warning);
          --dbms_output.put_line('Contact Person Id' || ln_ctr_person_id);
          --dbms_output.put_line('Out Contact API');
        EXCEPTION
          when others then
            lc_err_flag      := 'E';
            ln_err_cnt       := ln_err_cnt + 1;
            lc_error_message := lc_error_message ||
                                ' Employee Contact Api Error: ' || SQLERRM;
        END;
        /*        if ln_err_cnt = 0 and i.phone_number is not null then
          Begin
            -- Create Employee Contact Phone Detail
            -- -----------------------------------------------------------
              --dbms_output.put_line('In Phone API');
              hr_phone_api.create_phone( -- Input data elements
                                         -- -----------------------------
                                         p_date_from      => TO_DATE(i.phone_start_date,
                                                                     'DD-MON-RRRR'),
                                         p_phone_type     => lc_phone_type,
                                         p_phone_number   => i.phone_number,
                                         p_parent_id      => ln_ctr_person_id,
                                         p_parent_table   => 'PER_ALL_PEOPLE_F',
                                         p_effective_date => TO_DATE(i.phone_start_date,
                                                                     'DD-MON-RRRR'),
                                         -- Output data elements
                                         -- --------------------------------
                                          p_phone_id              => ln_phone_id,
                                          p_object_version_number => ln_ph_object_version_number);
                --dbms_output.put_line('Out Phone API');
           Exception
              when others then
                lc_err_flag      := 'E';
                ln_err_cnt       := ln_err_cnt + 1;
                lc_error_message := lc_error_message ||
                                    ' Contact Phone Api Error: ' || SQLERRM;
            End;
        End if;*/
        /*        if ln_err_cnt = 0 and i.address_line1 is not null then
          Begin
            -- Call Employee Contact API Procedure
            --dbms_output.put_line('In Address API');
            hr_person_address_api.create_person_address(p_validate       => false,
                                                        p_effective_date => i.start_date,
                                                        p_person_id      => ln_ctr_person_id,
                                                        p_style          => 'PK_GLB',
                                                        p_primary_flag   => 'Y',
                                                        --p_address_type          => V_ADDRESS_TYPE,
                                                        p_address_line1         => i.address_line1,
                                                        p_address_line2         => i.address_line2, --||' '||i.address_line3,
                                                        p_address_line3         => null,
                                                        p_town_or_city          => initcap(i.town_or_city),
                                                        p_add_information13     => i.tehsil,
                                                        p_add_information14     => i.district,
                                                        p_region_1              => i.addr_region,
                                                        p_add_information15     => i.province,
                                                        p_country               => V_COUNTRY,
                                                        p_postal_code           => i.postal_code,
                                                        p_telephone_number_1    => i.phone_no1,
                                                        p_telephone_number_2    => i.phone_no2,
                                                        p_telephone_number_3    => i.mobile_no1,
                                                        p_add_information18     => i.mobile_no2,
                                                        p_add_information16     => i.fax,
                                                        p_add_information17     => i.email,
                                                        p_date_from             => i.start_date,
                                                        p_date_to               => i.addr_date_to,
                                                        p_address_id            => ln_address_id,
                                                        p_object_version_number => ln_addr_object_version_number);
            -- dbms_output.put_line('Out Address API');
          EXCEPTION
            when others then
              lc_err_flag      := 'E';
              ln_err_cnt       := ln_err_cnt + 1;
              lc_error_message := lc_error_message ||
                                  ' Contact Address Api Error: ' || SQLERRM;
          END;
        End if;
        */
        IF ln_err_cnt >= 1 THEN
          rollback;
          UPDATE XXHBL_EMP_CONTACT_STG
             SET status = lc_err_flag, error_msg = lc_error_message
           WHERE record_id = i.record_id;
          commit;
        ELSE
          UPDATE XXHBL_EMP_CONTACT_STG
             SET status     = 'S',
                 contact_id = ln_ctr_relationship_id,
                 error_msg  = ''
           WHERE record_id = i.record_id;
          commit;
        END IF;
      else
        UPDATE XXHBL_EMP_CONTACT_STG
           SET status = lc_err_flag, error_msg = lc_error_message
         WHERE record_id = i.record_id;
        commit;
      end if;
      lc_err_flag      := null;
      ln_err_cnt       := 0;
      lc_error_message := null;
    END LOOP;
    --COMMIT;
  exception
    WHEN OTHERS THEN
      ROLLBACK;
      dbms_output.put_line(SQLERRM);
  end create_emp_contact;

  procedure create_med_assesment is
  
    l_medical_assessment_id number;
    l_object_version_number number;
  
    v_err_msg  varchar2(500) := null;
    ln_err_cnt number := 0;
    V_STATUS   varchar2(10) := null;
    v_bg       varchar2(10);
  
    CURSOR EMP_RECORDS IS
      select *
        from XXhBL_EMPLOYEE_STG
       WHERE 1 = 1
         and NVL(STATUS, 'S') = 'S'
            --and record_id=346  --345  --335  --334
         and trim(blood_group) is not null
       ORDER BY RECORD_ID;
  
  Begin
    ------------------------------------------------
    for rec in EMP_RECORDS LOOP
      --in select * from X
    
      Begin
        SELECT A.FLEX_VALUE
          into v_bg
          FROM FND_FLEX_VALUES A
         WHERE A.FLEX_VALUE_SET_ID = 1016227
           AND A.FLEX_VALUE = rec.blood_group;
      
      Exception
        When NO_DATA_FOUND THEN
          V_STATUS   := 'E';
          ln_err_cnt := ln_err_cnt + 1;
          V_ERR_MSG  := V_ERR_MSG || ' Blood group Not Found.';
          --          dbms_output.put_line(' Gender Not Found.');
      END;
    
      if ln_err_cnt = 0 then
      
        begin
          per_medical_assessment_api.create_medical_assessment(p_validate              => FALSE,
                                                               p_effective_date        => rec.date_of_joining,
                                                               p_person_id             => rec.person_id,
                                                               p_consultation_date     => rec.date_of_joining,
                                                               p_consultation_type     => 'BG' --nvl(rec.type,'AM')
                                                              ,
                                                               p_consultation_result   => '' --rec.result
                                                              ,
                                                               p_attribute1            => rec.blood_group,
                                                               p_medical_assessment_id => l_medical_assessment_id,
                                                               p_object_version_number => l_object_version_number);
        
          UPDATE XXHBL_EMPLOYEE_STG
             SET status = 'SS', error_msg = V_ERR_MSG
           WHERE record_id = rec.record_id;
        
        Exception
          WHEN OTHERS THEN
            V_ERR_MSG := V_ERR_MSG || ' ' || SQLCODE || '.' || SQLERRM;
            UPDATE XXHBL_EMPLOYEE_STG
               SET status = 'E', error_msg = V_ERR_MSG
             WHERE record_id = rec.record_id;
          
            dbms_output.put_line('Exception in API');
            v_err_msg  := null;
            ln_err_cnt := 0;
            V_STATUS   := null;
        End;
      
      Else
        UPDATE XXHBL_EMPLOYEE_STG
           SET status = 'E', error_msg = V_ERR_MSG
         WHERE record_id = rec.record_id;
      End if;
    
    end loop;
  
  end;

  procedure UPDATE_employee is
    l_person_id                 per_people_f.person_id%type;
    V_EMPLOYEE_NUMBER           VARCHAR2(200);
    l_assignment_sequence       per_assignments_f.assignment_sequence%type;
    l_assignment_number         per_assignments_f.assignment_number%type;
    l_assignment_id             number;
    l_per_object_version_number number;
    l_asg_object_version_number number;
    l_per_effective_start_date  date;
    l_per_effective_end_date    date;
    l_full_name                 per_people_f.full_name%type;
    l_per_comment_id            number;
    l_name_combination_warning  boolean;
    l_assign_payroll_warning    boolean;
    L_ORIG_HIRE_WARNING         BOOLEAN;
    l_object_version_number     number;
    v_object_version_number     number;
    V_GENDER                    VARCHAR2(100);
    V_BUSINESS_GROUP_ID         NUMBER;
    V_PERSON_TYPE_ID            NUMBER;
    V_MARITAL_STATUS            VARCHAR2(100);
    V_NATIONALITY               VARCHAR2(100);
    V_COUNTRY                   VARCHAR2(100);
    V_BLOOD_GROUP               VARCHAR2(100);
    V_TITLE                     VARCHAR2(100);
    V_STATUS                    VARCHAR2(10);
    V_ERR_MSG                   VARCHAR2(500);
    ln_err_cnt                  number := 0;
    V_PERSON_ID                 number;
    v_start_date                date;
    CURSOR EMP_RECORDS IS
      SELECT E.record_id,
             e.business_group,
             e.gender,
             e.person_type,
             E.BLOOD_GROUP,
             e.marital_status,
             e.nationality,
             e.country,
             e.employee_number,
             e.title,
             e.date_of_joining,
             nvl(trim(e.last_name), '.') last_name,
             e.first_name,
             e.middle_name,
             e.full_name,
             e.brith_date,
             e.email_address,
             e.cnic,
             e.town_of_birth,
             E.RELIGION,
             E.NTN_NO
        FROM XXHBL_EMPLOYEE_STG E
      --WHERE E.record_id between 602 and 825;
      --and e.employee_number='449482';
       WHERE trim(E.LAST_NAME) is not null
         AND E.EMPLOYEE_NUMBER NOT IN
             (select distinct empno
                from xxhbl_assignment_history_
               where rec_status = 'DD');
  
  Begin
    ------------------------------------------------
    for i in EMP_RECORDS LOOP
      --in select * from XXUBL_EMPLOYEE_STG
      -------------------------------------------------
      Begin
        select HAOU.BUSINESS_GROUP_ID
          INTO V_BUSINESS_GROUP_ID
          from HR_ALL_ORGANIZATION_UNITS HAOU
         WHERE HAOU.TYPE = 'BG' -- 10
           and HAOU.NAME = I.BUSINESS_GROUP;
        --dbms_output.put_line('Business Group id.' || V_BUSINESS_GROUP_ID);
      Exception
        When NO_DATA_FOUND THEN
          V_STATUS   := 'E';
          ln_err_cnt := ln_err_cnt + 1;
          V_ERR_MSG  := V_ERR_MSG || ' Business Group Not Found.';
          dbms_output.put_line(' Business Group Not Found.');
      END;
      -------------------------------------------------
      Begin
        select DISTINCT lv.lookup_code
          INTO V_GENDER
          from Fnd_Lookup_ValueS LV
         WHERE LV.lookup_type = 'SEX'
           and upper(lv.meaning) = upper(I.GENDER);
      Exception
        When NO_DATA_FOUND THEN
          V_STATUS   := 'E';
          ln_err_cnt := ln_err_cnt + 1;
          V_ERR_MSG  := V_ERR_MSG || ' Gender Not Found.';
          dbms_output.put_line(' Gender Not Found.');
      END;
      -------------------------------------------------
      Begin
        SELECT DISTINCT PPT.PERSON_TYPE_ID
          INTO V_PERSON_TYPE_ID
          FROM per_person_types PPT
         WHERE upper(PPT.USER_PERSON_TYPE) = upper(I.PERSON_TYPE)
           AND PPT.BUSINESS_GROUP_ID =
               (SELECT BG.business_group_id
                  FROM PER_BUSINESS_GROUPS BG
                 WHERE BG.name = I.BUSINESS_GROUP);
        --dbms_output.put_line('Person Type id.' || V_person_type_id);
      Exception
        When NO_DATA_FOUND THEN
          V_STATUS   := 'E';
          ln_err_cnt := ln_err_cnt + 1;
          V_ERR_MSG  := V_ERR_MSG || ' Person Type Not Found.';
          dbms_output.put_line('Person Not Found.');
      END;
      -------------------------------------------------
      If i.marital_status is not null then
        Begin
          SELECT DISTINCT LOOKUP_CODE
            INTO V_MARITAL_STATUS
            FROM FND_LOOKUP_VALUES FLV
           WHERE FLV.LOOKUP_TYPE LIKE 'MAR_STATUS'
             AND UPPER(FLV.MEANING) = UPPER(I.MARITAL_STATUS);
        Exception
          When NO_DATA_FOUND THEN
            V_STATUS   := 'E';
            ln_err_cnt := ln_err_cnt + 1;
            V_ERR_MSG  := V_ERR_MSG || ' Marital Status Not Found.';
            dbms_output.put_line('Marital Status Not Found.');
        END;
      End if;
      -------------------------------------------------
      if i.nationality is not null then
        Begin
          SELECT DISTINCT LV.LOOKUP_CODE
            INTO V_NATIONALITY
            FROM FND_LOOKUP_VALUES LV
           WHERE LV.LOOKUP_TYPE = 'NATIONALITY'
             AND UPPER(LV.MEANING) = UPPER(I.NATIONALITY);
        Exception
          When NO_DATA_FOUND THEN
            V_STATUS   := 'E';
            ln_err_cnt := ln_err_cnt + 1;
            V_ERR_MSG  := V_ERR_MSG || ' Nationality Not Found.';
            dbms_output.put_line('Nationality Not Found.');
        END;
      end if;
      -------------------------------------------------
      If i.country is not null then
        Begin
          SELECT FTV.TERRITORY_CODE
            INTO V_COUNTRY
            FROM FND_TERRITORIES_VL FTV
           WHERE UPPER(FTV.TERRITORY_SHORT_NAME) = UPPER(I.COUNTRY);
        Exception
          When NO_DATA_FOUND THEN
            V_STATUS   := 'E';
            ln_err_cnt := ln_err_cnt + 1;
            V_ERR_MSG  := V_ERR_MSG || ' Country Not Found.';
            dbms_output.put_line('Country Not Found.');
        END;
      End if;
      -------------------------------------------------
      If i.Blood_Group != 'ZZZ' then
        if i.blood_GROUP is not null then
          Begin
            SELECT DISTINCT LV.LOOKUP_CODE
              INTO V_BLOOD_GROUP
              FROM FND_LOOKUP_VALUES LV
             WHERE LV.LOOKUP_TYPE = 'BLOOD_TYPE'
               AND UPPER(LV.LOOKUP_CODE) = UPPER(I.BLOOD_GROUP);
          Exception
            When NO_DATA_FOUND THEN
              V_STATUS   := 'E';
              ln_err_cnt := ln_err_cnt + 1;
              V_ERR_MSG  := V_ERR_MSG || ' Blood Group Not Found.';
              dbms_output.put_line('Blood Group Not Found.');
          END;
        end if;
      End if;
      ------------------------------------------------
      Begin
        V_PERSON_ID := NULL;
        SELECT PERSON_ID, OBJECT_VERSION_NUMBER
          INTO V_PERSON_ID, V_OBJECT_VERSION_NUMBER
          FROM PER_ALL_PEOPLE_F PAPF
         WHERE PERSON_TYPE_ID IN (1126)
           AND PAPF.EMPLOYEE_NUMBER = I.EMPLOYEE_NUMBER
           AND PAPF.EFFECTIVE_START_DATE = i.date_of_joining;
      Exception
        When NO_DATA_FOUND THEN
          NULL;
      END;
      -------------------------------------------------
      Begin
        SELECT DISTINCT LV.LOOKUP_CODE
          INTO V_TITLE
          FROM FND_LOOKUP_VALUES LV
         WHERE LV.LOOKUP_TYPE = 'TITLE'
           AND UPPER(LV.MEANING) = UPPER(I.TITLE);
      Exception
        When NO_DATA_FOUND THEN
          V_STATUS   := 'E';
          ln_err_cnt := ln_err_cnt + 1;
          V_ERR_MSG  := V_ERR_MSG || 'Title Not Found.';
          dbms_output.put_line('Title Not Found.');
      END;
      -------------------------------------------------
      if ln_err_cnt = 0 then
        begin
          IF V_PERSON_ID IS not NULL THEN
            SELECT PERSON_ID, OBJECT_VERSION_NUMBER, start_Date
              INTO V_PERSON_ID, V_OBJECT_VERSION_NUMBER, v_start_date
              FROM PER_ALL_PEOPLE_F PAPF
             WHERE PERSON_TYPE_ID IN (1126)
               AND PAPF.EMPLOYEE_NUMBER = I.EMPLOYEE_NUMBER
               AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE AND
                   PAPF.EFFECTIVE_END_DATE;
            -- Call the procedure
            /*          HR_PERSON_API.UPDATE_PERSON(
                              P_VALIDATE                      =>       FALSE ,
                              P_EFFECTIVE_DATE                =>       I.Date_Of_Joining,
                              P_DATETRACK_UPDATE_MODE          =>       'UPDATE' ,
                              P_PERSON_ID                      =>       V_PERSON_ID,
                              P_OBJECT_VERSION_NUMBER          =>       v_object_version_number,
                              P_PERSON_TYPE_ID                =>       V_PERSON_TYPE_ID,
                              P_LAST_NAME                      =>       I.LAST_NAME,
                              P_DATE_OF_BIRTH                  =>       I.BRITH_DATE,
                              P_EMAIL_ADDRESS                  =>       ltrim(I.Email_Address),
                              P_EMPLOYEE_NUMBER                =>       I.EMPLOYEE_NUMBER,
                              P_FIRST_NAME                    =>       I.FIRST_NAME,
                              P_MARITAL_STATUS                =>       V_MARITAL_STATUS,
                              P_MIDDLE_NAMES                  =>       I.MIDDLE_NAME,
                              P_NATIONALITY                    =>       V_NATIONALITY,
                              P_NATIONAL_IDENTIFIER            =>       I.CNIC,
                              p_registered_disabled_flag      =>      'N',
                              p_background_check_status       =>      'N',
                              p_on_military_service           =>      'N',
                              p_resume_exists                 =>      'N',
                              P_SEX                            =>       V_GENDER,
                              P_TITLE                          =>       V_TITLE,
                              p_attribute1                    =>      I.OLD_EMPLOYEE_NUMBER,
                              p_attribute2                    =>      i.Old_Nic,
                              p_attribute3                    =>      I.NTN,
                              p_attribute4                    =>      initcap(i.Religion),
                              p_attribute5                    =>      i.Spi_Eligibility,
                              p_attribute6                    =>      i.Medical_Eligibility,
                              p_attribute7                    =>      i.Domicile,
                              p_attribute8                    =>      i.City_Of_Birth,
                              p_attribute9                    =>      i.Ailment,
                              p_attribute10                   =>      i.Symbol_Customer_No,
                              p_attribute11                   =>      i.Nic_Expiry_Date,
                              P_DATE_OF_DEATH                  =>       I.DATE_OF_DEATH,
                              P_BLOOD_TYPE                    =>       V_BLOOD_GROUP,
                              P_TOWN_OF_BIRTH                 =>       I.TOWN_OF_BIRTH,
                              P_COUNTRY_OF_BIRTH             =>       V_COUNTRY,
                              P_EFFECTIVE_START_DATE         =>       l_per_effective_start_date,
                              P_EFFECTIVE_END_DATE           =>       l_per_effective_end_date,
                              P_FULL_NAME                     =>       l_full_name,
                              P_COMMENT_ID                   =>       l_per_comment_id,
                              P_NAME_COMBINATION_WARNING     =>       l_name_combination_warning,
                              P_ASSIGN_PAYROLL_WARNING       =>       l_assign_payroll_warning,
                              P_ORIG_HIRE_WARNING             =>       L_ORIG_HIRE_WARNING
                       );
              ELSIF V_PERSON_ID IS NOT NULL
              THEN
            */
            -- Call the procedure
            HR_PERSON_API.UPDATE_PERSON(P_VALIDATE              => FALSE,
                                        P_EFFECTIVE_DATE        => I.Date_Of_Joining,
                                        P_DATETRACK_UPDATE_MODE => 'CORRECTION',
                                        P_PERSON_ID             => V_PERSON_ID,
                                        P_OBJECT_VERSION_NUMBER => v_object_version_number,
                                        P_PERSON_TYPE_ID        => V_PERSON_TYPE_ID,
                                        --P_LAST_NAME                      =>       I.LAST_NAME,
                                        --                        P_DATE_OF_BIRTH                  =>       I.BRITH_DATE,
                                        --                        P_EMAIL_ADDRESS                  =>       ltrim(I.Email_Address),
                                        P_EMPLOYEE_NUMBER => I.EMPLOYEE_NUMBER,
                                        --                        P_FIRST_NAME                    =>       I.FIRST_NAME,
                                        --                        P_MARITAL_STATUS                =>       V_MARITAL_STATUS,
                                        --                        P_MIDDLE_NAMES                  =>       I.MIDDLE_NAME,
                                        P_NATIONALITY => V_NATIONALITY,
                                        --                        P_NATIONAL_IDENTIFIER            =>       I.CNIC,
                                        --                        p_registered_disabled_flag      =>      'N',
                                        --                        p_background_check_status       =>      'N',
                                        --                        p_on_military_service           =>      'N',
                                        --                        p_resume_exists                 =>      'N',
                                        --                        P_SEX                            =>       V_GENDER,
                                        --                        P_TITLE                          =>       V_TITLE,
                                        --                        p_attribute1                    =>      I.OLD_EMPLOYEE_NUMBER,
                                        ---p_attribute2                    =>      i.Religion,
                                        ---p_attribute3                    =>      I.NTN_NO,
                                        /*                        p_attribute4                    =>      initcap(i.Religion),
                                                                p_attribute5                    =>      i.Spi_Eligibility,
                                                                p_attribute6                    =>      i.Medical_Eligibility,
                                                                p_attribute7                    =>      i.Domicile,
                                                                p_attribute8                    =>      i.City_Of_Birth,
                                                                p_attribute9                    =>      i.Ailment,
                                                                p_attribute10                   =>      i.Symbol_Customer_No,
                                                                p_attribute11                   =>      i.Nic_Expiry_Date,
                                                                P_DATE_OF_DEATH                  =>       I.DATE_OF_DEATH,
                                                                P_BLOOD_TYPE                    =>       V_BLOOD_GROUP,
                                        --                        P_TOWN_OF_BIRTH                 =>       I.TOWN_OF_BIRTH,
                                        --                        P_COUNTRY_OF_BIRTH             =>       V_COUNTRY,*/
                                        P_EFFECTIVE_START_DATE     => l_per_effective_start_date,
                                        P_EFFECTIVE_END_DATE       => l_per_effective_end_date,
                                        P_FULL_NAME                => l_full_name,
                                        P_COMMENT_ID               => l_per_comment_id,
                                        P_NAME_COMBINATION_WARNING => l_name_combination_warning,
                                        P_ASSIGN_PAYROLL_WARNING   => l_assign_payroll_warning,
                                        P_ORIG_HIRE_WARNING        => L_ORIG_HIRE_WARNING);
            update xxhbl_employee_stg s
               set s.status = 'S'
             WHERE s.record_id = i.record_id
               and s.employee_number = i.employee_number;
          
          END IF;
        
          /*UPDATE xxhbl_blood_group
             SET status = 'S', error_msg = ' ',person_id = V_PERSON_ID
           WHERE record_id = i.record_id;
          dbms_output.put_line(SQLERRM);*/
        EXCEPTION
          WHEN OTHERS THEN
            /*V_ERR_MSG := V_ERR_MSG || ' ' || SQLCODE || '.' || SQLERRM;
            UPDATE xxhbl_blood_group
               SET status = 'E', error_msg = V_ERR_MSG
             WHERE record_id = i.record_id;
            dbms_output.put_line('Exception in API');
            v_err_msg  := null;
            ln_err_cnt := 0;
            V_STATUS   := null;*/
          
            update xxhbl_employee_stg s
               set s.status = 'E'
             WHERE s.record_id = i.record_id
               and s.employee_number = i.employee_number;
          
            dbms_output.put_line('Exception in API');
        END;
      
      Else
        /*UPDATE xxhbl_blood_group
          SET status = 'E', error_msg = V_ERR_MSG
        WHERE record_id = i.record_id;*/
        update xxhbl_employee_stg s
           set s.status = 'E', S.ERROR_MSG = V_ERR_MSG
         WHERE s.record_id = i.record_id
           and s.employee_number = i.employee_number;
      
        --         dbms_output.put_line('Validation failed');
      
      End if;
    
      v_err_msg  := null;
      ln_err_cnt := 0;
      v_status   := null;
    
    END LOOP;
    COMMIT;
  exception
    when others then
      dbms_output.put_line('error : ' || sqlerrm);
  end UPDATE_employee;

  PROCEDURE update_assignments_criteria IS
    CURSOR cur_validate_data IS
    --sahn
      SELECT A.RECORD_ID,
             'HBL Pakistan' BUSINESS_GROUP,
             EMPNO EMPLOYEE_NUMBER,
             A.DATE_FROM FROM_DATE,
             A.DATE_TO TO_DATE,
             A.GRADE,
             A.JOB,
             A.POSTION POSITION,
             A.ORG_NAME ORGANIZATION,
             A.LOCATION,
             'Active Assignment' ASSIGNMENT_STATUS,
             'Permanent Confirm' ASSIGNMENT_CATEGORY,
             A.UPD_MODE,
             A.PROBATION_END_DATE,
             A.DURATION_UNIT
      
        FROM xxhbl_assignment_history_ A
       WHERE 1 = 1 --CHANGE_REASON=1
         AND REC_STATUS IS NULL
      --AND A.EMPNO LIKE '4%'
      --and a.upd_mode ='UPDATE_CHANGE_INSERT'
      --AND A.rec_status = '-DOC II'
      --and a.upd_mode in ('CORRECTION','UPDATE')
      --AND EMPNO  IN  ('331277')
       ORDER BY 3, 4;
    /*(
    select distinct empno
    --empno,q.ename,q.date_from,q.rec_status,q.record_id,q.upd_mode
    from xxhbl_assignment_history_ q
    where empno in (select empno from emp_temp)
    )*/
    --and UPD_MODE= 'CORRECTION'   --2) 'UPDATE'    --1) -'CORRECTION'   --3)'UPDATE_CHANGE_INSERT'
  
    /*SELECT * FROM xxhbl_assignments_stg
    WHERE NVL(status, 'N') = 'N'*/
    -- AND RECORD_ID=3
    /*AND RECORD_ID IN (SELECT S.RECORD_ID
    --,S.FROM_DATE,S.TO_DATE,A.*
    FROM XXUBL_ASSIGNMENTS_STG S,
                      PER_ALL_ASSIGNMENTS_F A,
                      PER_ALL_PEOPLE_F P
        WHERE S.RECORD_ID >=11005
        AND S.ASSIGNMENT_STATUS ='Active Assignment'
        AND S.FROM_DATE < '01-JAN-2016'
        AND S.EMPLOYEE_NUMBER= P.EMPLOYEE_NUMBER
        AND A.PERSON_ID = P.PERSON_ID
        AND P.BUSINESS_GROUP_ID=142
        AND TRUNC(SYSDATE) BETWEEN P.EFFECTIVE_START_DATE AND P.EFFECTIVE_END_DATE
        --AND TRUNC(SYSDATE) BETWEEN A.EFFECTIVE_START_DATE AND A.EFFECTIVE_END_DATE
        --AND S.FROM_DATE > A.EFFECTIVE_START_DATE
        AND S.FROM_DATE = A.EFFECTIVE_START_DATE
        AND S.TO_DATE = A.EFFECTIVE_END_DATE
        )
        --AND RECORD_ID =9739*/
    --                      order by record_id;
    /*select *
     from xxubl_assignments_stg a
    where a.record_id in
          (select s.record_id
           --,s.employee_number,s.from_date,s.to_date,pa.effective_start_date,pa.effective_end_date
             from xxubl_assignments_stg s,
                  per_all_assignments_f pa,
                  per_all_people_f      p
            where s.record_id >= 9447
              and s.status is null
              and pa.effective_start_date = s.from_date
              and pa.effective_end_date = s.to_date
              and s.employee_number = p.employee_number
              and p.person_id = pa.person_id
              and trunc(sysdate) between p.effective_start_date and
                  p.effective_end_date)
              --and a.record_id between 9481 and 9538
              order by 1;*/
    /*                 select *
     from xxubl_assignments_stg_3 a
    where a.record_id in
          (select s.record_id
           --,s.employee_number,s.from_date,s.to_date,pa.effective_start_date,pa.effective_end_date
             from xxubl_assignments_stg_3 s,
                  per_all_assignments_f pa,
                  per_all_people_f      p
            where \*s.record_id >= 9447
              and *\s.status IS NULL
              --AND error_msg like ' Assignment Not Found......%'
              and pa.effective_start_date = s.from_date
              and pa.effective_end_date = s.to_date
              and s.employee_number = p.employee_number
              and p.person_id = pa.person_id
              and trunc(sysdate) between p.effective_start_date and
                  p.effective_end_date)
              --and a.record_id between 9481 and 9538
              order by 1;*/
  
    lc_err_flag      char;
    ln_err_cnt       number := 0;
    lc_error_message varchar2(1000);
    --
    -- Declaration of package private cursors
    --
    /* Get Business Group id */
    CURSOR cur_business_group_id(p_business_group_name varchar2) IS
      SELECT BUSINESS_GROUP_ID
        FROM PER_BUSINESS_GROUPS
       WHERE UPPER(NAME) = UPPER(p_business_group_name);
    /* Get person id */
    CURSOR cur_person_id(p_emp_no varchar2, p_business_group_id number) IS
      SELECT PERSON_ID, original_date_of_hire
        FROM PER_ALL_PEOPLE_F
       WHERE EMPLOYEE_NUMBER = p_emp_no
         AND BUSINESS_GROUP_ID = p_business_group_id
         AND TRUNC(SYSDATE) BETWEEN EFFECTIVE_START_DATE AND
             EFFECTIVE_END_DATE
         AND CURRENT_EMPLOYEE_FLAG = 'Y';
    /* Get effective start date */
    CURSOR cur_effective_date(p_person_id number,
                              p_bg_id     number,
                              p_start     date,
                              p_end       date) IS
      SELECT EFFECTIVE_START_DATE
        FROM PER_ALL_ASSIGNMENTS_F
       WHERE PERSON_ID = p_person_id
         AND BUSINESS_GROUP_ID = p_bg_id
         AND /*TRUNC(SYSDATE)*/
             p_start BETWEEN EFFECTIVE_START_DATE AND EFFECTIVE_END_DATE
            --and p_start = EFFECTIVE_START_DATE
            --AND p_end = EFFECTIVE_END_DATE
         AND PRIMARY_FLAG = 'Y';
    /* Get assignment id */
    CURSOR cur_assignment_id(p_per_id    number,
                             p_bgroup_id number,
                             p_st_dt     date,
                             p_end_dt    date) IS
      SELECT ASSIGNMENT_ID, ASSIGNMENT_NUMBER, OBJECT_VERSION_NUMBER
        FROM PER_ALL_ASSIGNMENTS_F
       WHERE PERSON_ID = p_per_id
         AND BUSINESS_GROUP_ID = p_bgroup_id
         AND /*TRUNC(SYSDATE)*/
             p_st_dt BETWEEN EFFECTIVE_START_DATE AND EFFECTIVE_END_DATE
            --and p_st_dt = EFFECTIVE_START_DATE
            --AND p_end_dt = EFFECTIVE_END_DATE
         AND PRIMARY_FLAG = 'Y';
    /* Get Grade id */
    CURSOR cur_grade_id(p_grade_name varchar2, p_bg_id number) IS
      SELECT GRADE_ID
        FROM PER_GRADES
      --WHERE UPPER(NAME) = UPPER(p_grade_name)   --sahn
       WHERE REPLACE(UPPER(NAME), '\', '') = UPPER(p_grade_name)
         AND BUSINESS_GROUP_ID = p_bg_id;
    /* Get position id */
    CURSOR cur_position_id(p_position_name varchar2, p_bg_id number) IS
      SELECT p.POSITION_ID
        FROM HR_ALL_POSITIONS_F p, per_position_definitions ppd
       WHERE ppd.position_definition_id = p.position_definition_id
         and UPPER(ppd.segment1) || '-' || upper(ppd.segment2) =
             UPPER(p_position_name)
         AND p.BUSINESS_GROUP_ID = p_bg_id;
    /* Get job id */
    CURSOR cur_job_id(p_job_name varchar2, p_bg_id number) IS
      SELECT JOB_ID
        FROM PER_JOBS
       WHERE UPPER(replace(NAME, '\', '')) = UPPER(p_job_name)
         AND BUSINESS_GROUP_ID = p_bg_id;
    /* Get Location id */
    CURSOR cur_location_id(p_location_code varchar2) IS
      SELECT LOCATION_ID
        FROM HR_LOCATIONS_ALL_TL
       WHERE trim /*UPPER*/
             (LOCATION_CODE) = trim /*UPPER*/
             (p_location_code);
    /* Get Organization id */
    CURSOR cur_organization_id(p_organization_name varchar2,
                               p_bg_id             number) IS
      SELECT ORGANIZATION_ID
        FROM HR_ORGANIZATION_UNITS
       WHERE UPPER(NAME) = UPPER(p_organization_name)
         AND BUSINESS_GROUP_ID = p_bg_id;
    /* Get payroll id */
    CURSOR cur_payroll_id(p_payroll_name varchar2) IS
      SELECT PAYROLL_ID
        FROM PAY_ALL_PAYROLLS_F
       WHERE UPPER(PAYROLL_NAME) = UPPER(p_payroll_name);
    /* Get Supervisor id */
    CURSOR cur_supervisor_id(p_supervisor_number varchar2,
                             p_bgroup_id         number) IS
      SELECT PERSON_ID
        FROM PER_ALL_PEOPLE_F
       WHERE EMPLOYEE_NUMBER = p_supervisor_number
         AND TRUNC(SYSDATE) BETWEEN EFFECTIVE_START_DATE AND
             EFFECTIVE_END_DATE
         AND BUSINESS_GROUP_ID = p_bgroup_id;
    /* Assignments Type Status */
    CURSOR cur_ass_status_type_id(p_ass_status_type varchar2) IS
      SELECT assignment_status_type_id
        FROM per_assignment_status_types
       WHERE user_status = p_ass_status_type;
    /* Assignments Category */
    CURSOR cur_ass_category(p_category_code varchar2) IS
      SELECT lookup_code
        FROM fnd_lookup_values_vl
       WHERE lookup_type = 'EMP_CAT'
         AND UPPER(meaning) LIKE UPPER(p_category_code);
    /*  Pay Group */
    CURSOR cur_pay_group_id(p_pf             varchar2,
                            p_converted      varchar2,
                            p_converted_date varchar2,
                            p_graduaty       varchar2,
                            p_gpf            varchar2,
                            p_pension        varchar2,
                            p_compension     varchar2,
                            p_medical        varchar2,
                            p_bf             varchar2,
                            p_relocation     varchar2,
                            p_union          varchar2) IS
      SELECT people_group_id
        FROM pay_people_groups ppg
       where nvl(ppg.segment1, '-') = nvl(p_pf, '-')
         and nvl(ppg.segment2, '-') = nvl(p_converted, '-')
         and nvl(ppg.segment3, '-') = nvl(p_converted_date, '-')
         and nvl(ppg.segment4, '-') = nvl(p_graduaty, '-')
         and nvl(ppg.segment5, '-') = nvl(p_gpf, '-')
         and nvl(ppg.segment6, '-') = nvl(p_pension, '-')
         and nvl(ppg.segment7, '-') = nvl(p_compension, '-')
         and nvl(ppg.segment8, '-') = nvl(p_medical, '-')
         and nvl(ppg.segment9, '-') = nvl(p_bf, '-')
         and nvl(ppg.segment10, '-') = nvl(p_relocation, '-')
         and nvl(ppg.segment12, '-') = nvl(p_union, '-');
    /* Probation Unit */
    CURSOR cur_probation(p_probation varchar2) IS
      SELECT lookup_code
        FROM hr_lookups
       WHERE lookup_type = 'QUALIFYING_UNITS'
         AND UPPER(meaning) LIKE UPPER(p_probation);
    ln_assignment_id          number;
    ln_object_version_number  number;
    ln_obj_version_number     number;
    ld_effective_date         date;
    ln_person_id              number;
    ln_business_group_id      number;
    ld_hiredate               date;
    lc_mode                   varchar2(30);
    ln_job_id                 number;
    ln_grade_id               number;
    ln_position_id            number;
    ln_location_id            number;
    ln_organization_id        number;
    ln_payroll_id             number;
    ln_supervisor_id          number;
    ln_ass_status_type_id     number;
    ln_no_of_record           number := 0;
    ln_no_of_record_failure   number := 0;
    ln_no_of_record_success   number := 0;
    ln_no_of_record_updated   number := 0;
    ln_no_of_record_corrected number := 0;
    lc_ass_category_code      varchar2(30);
    lc_assignment_number      varchar2(30);
    ln_comment_id             number;
    ld_effective_start_date   date;
    ld_effective_end_date     date;
    lb_no_managers_warning    boolean;
    lc_concat_segments        varchar2(500);
    lc_probation_unit         varchar2(250);
    ln_people_grp             number;
    ln_eff_dt                 date;
    --out parameter
    ln_people_group_id            number;
    ln_special_ceiling_step_id    number;
    lc_group_name                 varchar2(500);
    lb_org_now_no_manager_warning boolean;
    lb_other_manager_warning      boolean;
    lb_spp_delete_warning         boolean;
    lc_entries_changed_warning    varchar2(100);
    lb_tax_dist_changed_warning   boolean;
    ll_comment_id                 per_all_assignments_f.comment_id%type;
    lc_concatenated_segments      hr_soft_coding_keyflex.concatenated_segments%type;
    lb_no_manager_warning         boolean;
    lc_soft_coding_keyflex_id     per_all_assignments_f.soft_coding_keyflex_id%type;
    ld_per_effective_start_date   PER_ALL_PEOPLE_F.EFFECTIVE_START_DATE%TYPE;
    ld_per_effective_end_date     PER_ALL_PEOPLE_F.EFFECTIVE_END_DATE%TYPE;
    lc_concat_segment             varchar2(250);
    ld_eff_start_date             date;
    ld_eff_end_date               date;
  BEGIN
    FOR i IN cur_validate_data LOOP
      --ln_no_of_record :=ln_no_of_record +1;
      lc_err_flag      := 'N';
      lc_error_message := NULL;
      /* Checking Business Group id */
      OPEN cur_business_group_id(i.business_group);
      FETCH cur_business_group_id
        INTO ln_business_group_id;
      IF (cur_business_group_id%NOTFOUND) THEN
        lc_err_flag      := 'E';
        ln_err_cnt       := ln_err_cnt + 1;
        lc_error_message := lc_error_message ||
                            ' Employee Business Group Not Found.';
      END IF;
      --dbms_output.put_line('Business Group id Error flag' || lc_err_flag);
      CLOSE cur_business_group_id;
      --   Checking Person id
      OPEN cur_person_id(i.employee_number, ln_business_group_id);
      FETCH cur_person_id
        INTO ln_person_id, ld_hiredate;
      IF (cur_person_id%NOTFOUND) THEN
        lc_err_flag      := 'E';
        ln_err_cnt       := ln_err_cnt + 1;
        lc_error_message := lc_error_message || ' Employee Not Found.';
      END IF;
      --dbms_output.put_line('Person id Error flag' || lc_err_flag);
      CLOSE cur_person_id;
      /* Checking Effective Date */
      OPEN cur_effective_date(ln_person_id,
                              ln_business_group_id,
                              i.from_date,
                              i.to_date);
      FETCH cur_effective_date
        INTO ld_effective_date;
      IF (cur_effective_date%NOTFOUND) THEN
        lc_err_flag      := 'E';
        ln_err_cnt       := ln_err_cnt + 1;
        lc_error_message := lc_error_message ||
                            ' Employee Effective Date Not Found.';
      END IF;
      --dbms_output.put_line('Effective Date Error flag' || lc_err_flag);
      CLOSE cur_effective_date;
      /* Checking Assignment id */
      OPEN cur_assignment_id(ln_person_id,
                             ln_business_group_id,
                             i.from_date,
                             i.to_date);
      FETCH cur_assignment_id
        INTO ln_assignment_id,
             lc_assignment_number,
             ln_object_version_number;
      IF (cur_assignment_id%NOTFOUND) THEN
        lc_err_flag      := 'E';
        ln_err_cnt       := ln_err_cnt + 1;
        lc_error_message := lc_error_message ||
                            ' Assignment Not Found......';
      END IF;
      --dbms_output.put_line('Assignment id Error flag' || lc_err_flag);
      CLOSE cur_assignment_id;
      /* Checking Grade id */
      IF (i.grade is not null) then
        OPEN cur_grade_id(i.grade, ln_business_group_id);
        FETCH cur_grade_id
          INTO ln_grade_id;
        IF (cur_grade_id%NOTFOUND) THEN
          lc_err_flag      := 'E';
          ln_err_cnt       := ln_err_cnt + 1;
          lc_error_message := lc_error_message ||
                              ' Employee Grade Not Found.';
        END IF;
        --dbms_output.put_line('Grade id Error flag' || lc_err_flag);
        CLOSE cur_grade_id;
      END IF;
      /* Checking Job id */
      if i.job is not null then
        OPEN cur_job_id(i.job, ln_business_group_id);
        FETCH cur_job_id
          INTO ln_job_id;
        IF (cur_job_id%NOTFOUND) THEN
          lc_err_flag      := 'E';
          ln_err_cnt       := ln_err_cnt + 1;
          lc_error_message := lc_error_message ||
                              ' Employee Job Not Found.';
        END IF;
        --dbms_output.put_line('Job id Error flag' || lc_err_flag);
        CLOSE cur_job_id;
      end if;
      /* Checking position id */
      IF i.position is not null then
        --sahn
        OPEN cur_position_id(i.position, ln_business_group_id);
        FETCH cur_position_id
          INTO ln_position_id;
        IF (cur_position_id%NOTFOUND) THEN
          lc_err_flag      := 'E';
          ln_err_cnt       := ln_err_cnt + 1;
          lc_error_message := lc_error_message ||
                              ' Employee Position Not Found.';
        END IF;
        --dbms_output.put_line('Position id Error flag' || lc_err_flag);
        CLOSE cur_position_id;
      END IF;
      /* Checking location_id */
      if i.location is not null then
        OPEN cur_location_id(i.location);
        FETCH cur_location_id
          INTO ln_location_id;
        IF (cur_location_id%NOTFOUND) THEN
          lc_err_flag      := 'E';
          ln_err_cnt       := ln_err_cnt + 1;
          lc_error_message := lc_error_message ||
                              ' Employee Location Not Found.';
        END IF;
        --dbms_output.put_line('Location id Error flag' || lc_err_flag);
        CLOSE cur_location_id;
      end if;
      /* Checking Organization id */
      IF (i.organization is not null) THEN
        OPEN cur_organization_id(i.organization, ln_business_group_id);
        FETCH cur_organization_id
          INTO ln_organization_id;
        IF (cur_organization_id%NOTFOUND) THEN
          lc_err_flag      := 'E';
          ln_err_cnt       := ln_err_cnt + 1;
          lc_error_message := lc_error_message ||
                              ' Employee Organization Not Found.';
        END IF;
        --dbms_output.put_line('Organization id Error flag' || lc_err_flag);
        CLOSE cur_organization_id;
      END IF;
      /* Checking Payroll id */
      /*
      IF (i.payroll is not null) THEN
        OPEN cur_payroll_id(i.payroll);
        FETCH cur_payroll_id
          INTO ln_payroll_id;
        IF (cur_payroll_id%NOTFOUND) THEN
          lc_err_flag      := 'E';
          ln_err_cnt       := ln_err_cnt + 1;
          lc_error_message := lc_error_message ||
                              ' Employee Payroll Not Found.';
        END IF;
        --dbms_output.put_line('Payroll id Error flag' || lc_err_flag);
        CLOSE cur_payroll_id;
      END IF;*/
      -- Checking Supervisor id
      /*   IF (i.supervisor_number is not null) THEN
        --  disable for loading people group
        OPEN cur_supervisor_id(i.supervisor_number, ln_business_group_id);
        FETCH cur_supervisor_id
          INTO ln_supervisor_id;
        IF (cur_supervisor_id%NOTFOUND) THEN
          lc_err_flag      := 'E';
          ln_err_cnt       := ln_err_cnt + 1;
          lc_error_message := lc_error_message ||
                              ' Employee Supervisor Not Found.';
        END IF;
        --dbms_output.put_line('Supervisor id Error flag' || lc_err_flag);
        CLOSE cur_supervisor_id;
      END IF;*/
      /* Checking Assignment Status Type */
      IF (i.assignment_status is not null) THEN
        OPEN cur_ass_status_type_id(i.assignment_status);
        FETCH cur_ass_status_type_id
          INTO ln_ass_status_type_id;
        IF (cur_ass_status_type_id%NOTFOUND) THEN
          lc_err_flag      := 'E';
          ln_err_cnt       := ln_err_cnt + 1;
          lc_error_message := lc_error_message ||
                              ' Assignment Status Type Not Found.';
        END IF;
        --dbms_output.put_line('Assignment Status Type Not Found' ||
        --   lc_err_flag);
        CLOSE cur_ass_status_type_id;
      END IF;
      /* Checking Assignment Category */
      IF (i.assignment_category is not null) THEN
        OPEN cur_ass_category(i.assignment_category);
        FETCH cur_ass_category
          INTO lc_ass_category_code;
        IF (cur_ass_category%NOTFOUND) THEN
          lc_err_flag      := 'E';
          ln_err_cnt       := ln_err_cnt + 1;
          lc_error_message := lc_error_message ||
                              ' Assignment Category Not Found.';
        END IF;
        --dbms_output.put_line('Assignment Category Not Found' ||
        --       lc_err_flag);
        CLOSE cur_ass_category;
      END IF;
      /* Checking Probation Unit */
      /*
        IF (i.duration_unit is not null) THEN
          --  disable for loading people group
          OPEN cur_probation(i.duration_unit);
          FETCH cur_probation
            INTO lc_probation_unit;
          IF (cur_probation%NOTFOUND) THEN
            lc_err_flag      := 'E';
            ln_err_cnt       := ln_err_cnt + 1;
            lc_error_message := lc_error_message ||
                                'Probation Unit Not Found.';
          END IF;
          CLOSE cur_probation;
        END IF;
      */
      /* Checking People Paygroup Id */
      --  IF (i.is_pf is not null and i.is_graduaty is not null) THEN
      /*     OPEN cur_pay_group_id(i.is_pf,
                            i.pf_converted,
                            i.pf_converted_date,
                            i.is_graduaty,
                            i.is_gpf,
                            i.is_pension,
                            i.compensation_absolute,
                            i.medical_benefits,
                            i.is_bf,
                            i.is_relocation,
                            i.is_union
                            );
      FETCH cur_pay_group_id
        INTO ln_people_grp; --ln_people_group_id;
      IF (cur_pay_group_id%NOTFOUND) THEN
        lc_err_flag      := 'E';
        ln_err_cnt       := ln_err_cnt + 1;
        lc_error_message := lc_error_message ||
                            ' People Group Combination Not Found.';
      END IF;
      --dbms_output.put_line('People Group Combination Not Found' ||
      --       lc_err_flag);
      CLOSE cur_pay_group_id;*/
      --      IF (TO_CHAR(ld_effective_date,'DD-MON-YYYY') = TO_CHAR(ld_hiredate,'DD-MON-YYYY')) THEN
      --          lc_mode := 'CORRECTION';
      --      ELSE
      --          lc_mode := 'UPDATE';
      --      END IF;
      --
      -- Call of Update Assignment Category
      --
      if ln_err_cnt = 0 then
        /*
        Begin
          hr_kflex_utility.ins_or_sel_keyflex_comb(p_appl_short_name => 'PAY',
                                                   p_flex_code       => 'GRP',
                                                   p_flex_num        => 50567,
                                                   p_segment1        => i.is_pf,
                                                   p_segment2        => i.pf_converted,
                                                   p_segment3        => i.pf_converted_date,
                                                   p_segment4        => i.is_graduaty,
                                                   p_segment5        => i.is_gpf,
                                                   p_segment6        => i.is_pension,
                                                   p_segment7        => i.compensation_absolute,
                                                   p_segment8        => i.medical_benefits,
                                                   p_segment9        => i.is_bf,
                                                   p_segment10       => i.is_relocation,
                                                   --  p_segment11                    => null,
                                                   p_segment12           => null, --nvl(i.is_union,null),
                                                   p_ccid                => ln_people_group_id,
                                                   p_concat_segments_out => lc_concat_segment);
          ln_people_grp := ln_people_group_id;
          dbms_output.put_line('Code Combination Id ' ||
                               ln_people_group_id || '' ||
                               lc_concat_segment);
          commit;
          IF ln_people_grp is null then
            lc_err_flag      := 'E';
            ln_err_cnt       := ln_err_cnt + 1;
            lc_error_message := lc_error_message ||
                                ' People Group Combination Not Found.';
          END IF;
        Exception
          when others then
            lc_err_flag      := 'E';
            ln_err_cnt       := ln_err_cnt + 1;
            lc_error_message := lc_error_message ||
                                'Error arises while creating new Pay People Group Combination.';
            dbms_output.put_line('Error arises while creating new Pay People Group Combination. ' ||
                                 SQLERRM);
            FND_FILE.PUT_LINE(FND_FILE.LOG,
                              'Error arises while creating new Pay People Group Combination.' ||
                              SQLERRM);
            FND_FILE.PUT_LINE(FND_FILE.LOG,
                              '                                                             ');
        End;   */
        --------------------
        Begin
          /*          SELECT PERSON_ID, original_date_of_hire
           into ln_person_id, ld_hiredate
           FROM PER_ALL_PEOPLE_F
          WHERE EMPLOYEE_NUMBER = i.employee_number
            AND BUSINESS_GROUP_ID = i.business_group
            AND \*TRUNC(SYSDATE)*\ i.from_date BETWEEN EFFECTIVE_START_DATE AND
                EFFECTIVE_END_DATE
            AND CURRENT_EMPLOYEE_FLAG = 'Y';
             SELECT EFFECTIVE_START_DATE
             into ld_effective_date
           FROM PER_ALL_ASSIGNMENTS_F
          WHERE assignment_id = i.assignment_id
            AND BUSINESS_GROUP_ID = i.business_group
            AND i.from_date BETWEEN EFFECTIVE_START_DATE AND
                EFFECTIVE_END_DATE
            AND PRIMARY_FLAG = 'Y';*/
          /*
            SELECT ASSIGNMENT_ID, ASSIGNMENT_NUMBER, OBJECT_VERSION_NUMBER
            INTO ln_assignment_id,
                lc_assignment_number,
                ln_object_version_number
           FROM PER_ALL_ASSIGNMENTS_F
          WHERE assignment_number = i.employee_number
            AND BUSINESS_GROUP_ID = i.business_group
            AND i.from_date BETWEEN EFFECTIVE_START_DATE AND
                EFFECTIVE_END_DATE
            AND PRIMARY_FLAG = 'Y';*/
          --asp
        
          hr_assignment_api.update_emp_asg_criteria(p_effective_date               => i.from_date,
                                                    p_datetrack_update_mode        => i.upd_mode, --'CORRECTION',  --'CORRECTION' /*'UPDATE_OVERRIDE'*/,--'CORRECTION',--'UPDATE_CHANGE_INSERT',
                                                    p_assignment_id                => ln_assignment_id,
                                                    p_validate                     => FALSE,
                                                    p_called_from_mass_update      => FALSE,
                                                    p_grade_id                     => ln_grade_id,
                                                    p_position_id                  => ln_position_id,
                                                    p_job_id                       => ln_job_id,
                                                    p_payroll_id                   => 61, --/*ln_payroll_id*/,
                                                    p_location_id                  => ln_location_id,
                                                    p_organization_id              => ln_organization_id,
                                                    p_pay_basis_id                 => null,
                                                    p_segment1                     => null,
                                                    p_segment2                     => null,
                                                    p_segment3                     => null,
                                                    p_segment4                     => null,
                                                    p_segment5                     => null,
                                                    p_segment6                     => null,
                                                    p_segment7                     => null,
                                                    p_segment8                     => null,
                                                    p_segment9                     => null,
                                                    p_segment10                    => null,
                                                    p_segment11                    => null,
                                                    p_segment12                    => null,
                                                    p_segment13                    => null,
                                                    p_segment14                    => null,
                                                    p_segment15                    => null,
                                                    p_segment16                    => null,
                                                    p_segment17                    => null,
                                                    p_segment18                    => null,
                                                    p_segment19                    => null,
                                                    p_segment20                    => null,
                                                    p_segment21                    => null,
                                                    p_segment22                    => null,
                                                    p_segment23                    => null,
                                                    p_segment24                    => null,
                                                    p_segment25                    => null,
                                                    p_segment26                    => null,
                                                    p_segment27                    => null,
                                                    p_segment28                    => null,
                                                    p_segment29                    => null,
                                                    p_segment30                    => null,
                                                    p_employment_category          => lc_ass_category_code,
                                                    p_concat_segments              => null,
                                                    p_grade_ladder_pgm_id          => null,
                                                    p_supervisor_assignment_id     => null,
                                                    p_people_group_id              => ln_people_grp /*ln_people_group_id*/,
                                                    p_object_version_number        => ln_object_version_number,
                                                    p_special_ceiling_step_id      => ln_special_ceiling_step_id,
                                                    p_group_name                   => lc_group_name,
                                                    p_effective_start_date         => ld_per_effective_start_date,
                                                    p_effective_end_date           => ld_per_effective_end_date,
                                                    p_org_now_no_manager_warning   => lb_org_now_no_manager_warning,
                                                    p_other_manager_warning        => lb_other_manager_warning,
                                                    p_spp_delete_warning           => lb_spp_delete_warning,
                                                    p_entries_changed_warning      => lc_entries_changed_warning,
                                                    p_tax_district_changed_warning => lb_tax_dist_changed_warning);
        
          update xxhbl_assignment_history_
             set rec_status  = 'CONF-S',
                 rec_remarks = 'Discsrded confirmation date row'
           where empno = i.employee_number
             and date_from = i.from_date;
        
        EXCEPTION
          WHEN OTHERS THEN
            dbms_output.put_line(sqlerrm);
            /* lc_err_flag      := 'E';       --sahn
            ln_err_cnt       := ln_err_cnt + 1;
            lc_error_message := 'Assignment Criteria Api Exception ' ||
                                SQLERRM;*/
            update xxhbl_assignment_history_
               set rec_status  = 'E',
                   rec_remarks = 'Assignment Criteria Api Exception'
             where empno = i.employee_number
               and date_from = i.from_date;
        End;
      
        --  disable for loading people group
        --if ln_err_cnt = 0 and i.supervisor_number is not null then
      
        if ln_err_cnt = 0 and i.probation_end_date is not null then
          Begin
            --
            -- Assignment Status Update API
            --
            /* hr_assignment_api.activate_emp_asg(p_validate                  => FALSE,
            p_effective_date            => i.from_date,
            p_datetrack_update_mode     => 'CORRECTION',
            p_assignment_id             => ln_assignment_id,
            p_object_version_number     => ln_object_version_number,
            p_assignment_status_type_id => ln_ass_status_type_id,
            p_effective_start_date      => ld_eff_start_date,
            p_effective_end_date        => ld_eff_end_date);*/
            --
            -- Call of Update Assignment Category
            --
          
            hr_assignment_api.update_emp_asg(p_validate              => FALSE,
                                             p_effective_date        => i.from_date, --trunc(sysdate), --
                                             p_datetrack_update_mode => i.upd_mode, ---'UPDATE',       --
                                             p_assignment_id         => ln_assignment_id,
                                             p_object_version_number => ln_object_version_number,
                                             p_supervisor_id         => null,
                                             p_assignment_number     => lc_assignment_number,
                                             --p_change_reason                => i.change_reason,
                                             p_comments           => null,
                                             p_date_probation_end => i.probation_end_date,
                                             p_probation_unit     => i.duration_unit,
                                             
                                             p_supervisor_assignment_id => null
                                             --Out Parameters
                                            ,
                                             p_concatenated_segments  => lc_concatenated_segments,
                                             p_soft_coding_keyflex_id => lc_soft_coding_keyflex_id,
                                             p_comment_id             => ln_comment_id,
                                             p_effective_start_date   => ld_effective_start_date,
                                             p_effective_end_date     => ld_effective_end_date,
                                             p_no_managers_warning    => lb_no_managers_warning,
                                             p_other_manager_warning  => lb_other_manager_warning);
          
            /*hr_assignment_api.update_emp_asg(p_validate                     => FALSE,
            p_effective_date               => i.from_date,
            p_datetrack_update_mode        => 'CORRECTION',
            p_assignment_id                => ln_assignment_id,
            p_object_version_number        => ln_object_version_number,
            p_supervisor_id                => ln_supervisor_id,
            p_assignment_number            => lc_assignment_number,
            p_change_reason                => i.change_reason,
            p_comments                     => null,
            --p_date_probation_end           => i.probation_end_date,
            p_default_code_comb_id         => null,
            p_frequency                    => null,
            p_internal_address_line        => null,
            p_manager_flag                 => null,
            p_normal_hours                 => null,
            p_perf_review_period           => null,
            p_perf_review_period_frequency => null,
            --p_probation_period             => I.PROBATION_DURATION,
            --p_probation_unit               => lc_probation_unit,
            p_sal_review_period            => null,
            p_sal_review_period_frequency  => null,
            p_set_of_books_id              => null,
            p_source_type                  => null,
            p_time_normal_finish           => null,
            p_time_normal_start            => null,
            p_bargaining_unit_code         => null,
            p_labour_union_member_flag     => null,
            p_hourly_salaried_code         => null,
            p_ass_attribute_category       => null,
            p_ass_attribute1               => null,
            p_ass_attribute2               => null,
            p_ass_attribute3               => null,
            p_ass_attribute4               => null,
            p_ass_attribute5               => null,
            p_ass_attribute6               => null,
            p_ass_attribute7               => null,
            p_ass_attribute8               => null,
            p_ass_attribute9               => null,
            p_ass_attribute10              => null,
            p_ass_attribute11              => null,
            p_ass_attribute12              => null,
            p_ass_attribute13              => null,
            p_ass_attribute14              => null,
            p_ass_attribute15              => null,
            p_ass_attribute16              => null,
            p_ass_attribute17              => null,
            p_ass_attribute18              => null,
            p_ass_attribute19              => null,
            p_ass_attribute20              => null,
            p_ass_attribute21              => null,
            p_ass_attribute22              => null,
            p_ass_attribute23              => null,
            p_ass_attribute24              => null,
            p_ass_attribute25              => null,
            p_ass_attribute26              => null,
            p_ass_attribute27              => null,
            p_ass_attribute28              => null,
            p_ass_attribute29              => null,
            p_ass_attribute30              => null,
            p_title                        => null,
            p_segment1                     => null,
            p_segment2                     => null,
            p_segment3                     => null,
            p_segment4                     => null,
            p_segment5                     => null,
            p_segment6                     => null,
            p_segment7                     => null,
            p_segment8                     => null,
            p_segment9                     => null,
            p_segment10                    => null,
            p_segment11                    => null,
            p_segment12                    => null,
            p_segment13                    => null,
            p_segment14                    => null,
            p_segment15                    => null,
            p_segment16                    => null,
            p_segment17                    => null,
            p_segment18                    => null,
            p_segment19                    => null,
            p_segment20                    => null,
            p_segment21                    => null,
            p_segment22                    => null,
            p_segment23                    => null,
            p_segment24                    => null,
            p_segment25                    => null,
            p_segment26                    => null,
            p_segment27                    => null,
            p_segment28                    => null,
            p_segment29                    => null,
            p_segment30                    => null,
            p_concat_segments              => null,
            p_supervisor_assignment_id     => null,
            --Out Parameters
            p_concatenated_segments  => lc_concatenated_segments,
            p_soft_coding_keyflex_id => lc_soft_coding_keyflex_id,
            p_comment_id             => ln_comment_id,
            p_effective_start_date   => ld_effective_start_date,
            p_effective_end_date     => ld_effective_end_date,
            p_no_managers_warning    => lb_no_managers_warning,
            p_other_manager_warning  => lb_other_manager_warning);*/
          EXCEPTION
            WHEN OTHERS THEN
              lc_err_flag      := 'E';
              ln_err_cnt       := ln_err_cnt + 1;
              lc_error_message := 'Assignment Api Exception ' || SQLERRM;
          End;
        
          /*Else
          UPDATE xxhbl_assignments_stg
             SET status = lc_err_flag, error_msg = lc_error_message
           WHERE record_id = i.record_id;*/
        End if;
      
        /*IF ln_err_cnt >= 1 THEN   --sahn  (for single assignment)
          rollback;
          UPDATE xxhbl_assignments_stg
             SET status = lc_err_flag, error_msg = lc_error_message
           WHERE record_id = i.record_id;
          commit;
        ELSE
          UPDATE xxhbl_assignments_stg
             SET status = 'S', assignment_id = ln_assignment_id
           WHERE record_id = i.record_id;
          commit;
        END IF;*/
      
        IF ln_err_cnt >= 1 THEN
          rollback;
          UPDATE xxhbl_assignment_history_
             SET rec_status = lc_err_flag, rec_remarks = lc_error_message
           WHERE empno = i.employee_number
             and date_from = i.from_date;
          commit;
        ELSE
          UPDATE xxhbl_assignment_history_
             SET rec_status = 'S', record_id = ln_assignment_id
           WHERE empno = i.employee_number
             and date_from = i.from_date;
          commit;
        END IF;
      
      ELSIF ln_err_cnt >= 1 THEN
        dbms_output.put_line(lc_error_message);
        /*UPDATE xxhbl_assignments_stg     --sahn
           SET status = lc_err_flag, error_msg = lc_error_message
         WHERE record_id = i.record_id;
        commit;*/
        UPDATE xxhbl_assignment_history_
           SET rec_status = 'E', rec_remarks = lc_error_message
         WHERE empno = i.employee_number
           and date_from = i.from_date;
        commit;
      end if;
      --
      lc_err_flag      := null;
      ln_err_cnt       := 0;
      lc_error_message := null;
    END LOOP;
    --commit;
  END update_assignments_criteria;
  -------------

  PROCEDURE update_assignments IS
    CURSOR cur_validate_data IS
      SELECT * FROM XXHBL_ASSIGNMENTS_STG WHERE NVL(status, 'N') = 'V';
  
    lc_validate_err_flag char;
    lc_error_message     varchar2(500);
  
    --
    -- Declaration of package private cursors
    --
  
    /* Get Business Group id */
  
    CURSOR cur_business_group_id(p_business_group_name varchar2) IS
      SELECT BUSINESS_GROUP_ID
        FROM PER_BUSINESS_GROUPS
       WHERE UPPER(NAME) = UPPER(p_business_group_name);
  
    /* Get person id */
  
    CURSOR cur_person_id(p_emp_no varchar2, p_business_group_id number) IS
      SELECT PERSON_ID, original_date_of_hire
        FROM PER_ALL_PEOPLE_F
       WHERE EMPLOYEE_NUMBER = p_emp_no
         AND BUSINESS_GROUP_ID = p_business_group_id
         AND TRUNC(SYSDATE) BETWEEN EFFECTIVE_START_DATE AND
             EFFECTIVE_END_DATE
         AND CURRENT_EMPLOYEE_FLAG = 'Y';
  
    /* Get effective start date */
  
    CURSOR cur_effective_date(p_person_id number, p_bg_id number) IS
      SELECT EFFECTIVE_START_DATE
        FROM PER_ALL_ASSIGNMENTS_F
       WHERE PERSON_ID = p_person_id
         AND BUSINESS_GROUP_ID = p_bg_id
         AND TRUNC(SYSDATE) BETWEEN EFFECTIVE_START_DATE AND
             EFFECTIVE_END_DATE
         AND PRIMARY_FLAG = 'Y';
  
    /* Get assignment id */
  
    CURSOR cur_assignment_id(p_per_id number, p_bgroup_id number) IS
      SELECT ASSIGNMENT_ID, ASSIGNMENT_NUMBER, OBJECT_VERSION_NUMBER
        FROM PER_ALL_ASSIGNMENTS_F
       WHERE PERSON_ID = p_per_id
         AND BUSINESS_GROUP_ID = p_bgroup_id
         AND TRUNC(SYSDATE) BETWEEN EFFECTIVE_START_DATE AND
             EFFECTIVE_END_DATE
         AND PRIMARY_FLAG = 'Y';
  
    ln_assignment_id         number;
    ln_object_version_number number;
    ld_effective_date        date;
    ln_person_id             number;
    ln_business_group_id     number;
    ld_hiredate              date;
    lc_mode                  varchar2(30);
    lc_assignment_number     varchar2(30);
  
    --out parameter
    lc_concatenated_segments  hr_soft_coding_keyflex.concatenated_segments%type;
    lc_soft_coding_keyflex_id per_all_assignments_f.soft_coding_keyflex_id%type;
    ln_comment_id             number;
    ld_effective_start_date   PER_ALL_PEOPLE_F.EFFECTIVE_START_DATE%TYPE;
    ld_effective_end_date     PER_ALL_PEOPLE_F.EFFECTIVE_END_DATE%TYPE;
    lb_no_managers_warning    boolean;
    lb_other_manager_warning  boolean;
  
  BEGIN
  
    FOR i IN cur_validate_data LOOP
    
      lc_validate_err_flag := 'N';
      lc_error_message     := NULL;
    
      /* Checking Business Group id */
    
      OPEN cur_business_group_id(i.business_group);
      FETCH cur_business_group_id
        INTO ln_business_group_id;
      IF (cur_business_group_id%NOTFOUND) THEN
        lc_validate_err_flag := 'Y';
        lc_error_message     := lc_error_message ||
                                ' Employee Business Group Not Found.';
      END IF;
      dbms_output.put_line('Business Group id Error flag' ||
                           lc_validate_err_flag);
      CLOSE cur_business_group_id;
    
      /* Checking Person id */
    
      OPEN cur_person_id(i.employee_number, ln_business_group_id);
      FETCH cur_person_id
        INTO ln_person_id, ld_hiredate;
      IF (cur_person_id%NOTFOUND) THEN
        lc_validate_err_flag := 'Y';
        lc_error_message     := lc_error_message || ' Employee Not Found.';
      END IF;
      dbms_output.put_line('Person id Error flag' || lc_validate_err_flag);
      CLOSE cur_person_id;
    
      /* Checking Effective Date */
    
      OPEN cur_effective_date(ln_person_id, ln_business_group_id);
      FETCH cur_effective_date
        INTO ld_effective_date;
      IF (cur_effective_date%NOTFOUND) THEN
        lc_validate_err_flag := 'Y';
        lc_error_message     := lc_error_message ||
                                ' Employee Effective Date Not Found.';
      END IF;
      dbms_output.put_line('Effective Date Error flag' ||
                           lc_validate_err_flag);
      CLOSE cur_effective_date;
    
      /* Checking Assignment id */
    
      OPEN cur_assignment_id(ln_person_id, ln_business_group_id);
      FETCH cur_assignment_id
        INTO ln_assignment_id,
             lc_assignment_number,
             ln_object_version_number;
      IF (cur_assignment_id%NOTFOUND) THEN
        lc_validate_err_flag := 'Y';
        lc_error_message     := lc_error_message ||
                                ' Assignment... Not Found.';
      END IF;
      dbms_output.put_line('Assignment id Error flag' ||
                           lc_validate_err_flag);
      CLOSE cur_assignment_id;
    
      /*IF (TO_CHAR(ld_effective_date,'DD-MON-YYYY') = TO_CHAR(ld_hiredate,'DD-MON-YYYY')) THEN
          lc_mode := 'CORRECTION';
      ELSE
          lc_mode := 'UPDATE';
      END IF; */
      --
      -- Call of Update Assignment Category
      --
      --hr_assignment_api.actual_termination_emp_asg(
    
      hr_assignment_api.update_emp_asg(p_validate              => FALSE,
                                       p_effective_date        => trunc(sysdate), --i.from_date
                                       p_datetrack_update_mode => 'UPDATE', --i.upd_mode
                                       p_assignment_id         => ln_assignment_id,
                                       p_object_version_number => ln_object_version_number,
                                       p_supervisor_id         => null,
                                       p_assignment_number     => lc_assignment_number,
                                       p_change_reason         => i.change_reason,
                                       p_comments              => null,
                                       p_date_probation_end    => i.probation_end_date,
                                       /*                                       p_default_code_comb_id         => null,
                                                                              p_frequency                    => null,
                                                                              p_internal_address_line        => null,
                                                                              p_manager_flag                 => null,
                                                                              p_normal_hours                 => null,
                                                                              p_perf_review_period           => null,
                                                                              p_perf_review_period_frequency => null,
                                                                              p_probation_period             => null,*/
                                       p_probation_unit => i.duration_unit,
                                       /*                                       p_sal_review_period            => null,
                                                                              p_sal_review_period_frequency  => null,
                                                                              p_set_of_books_id              => null,
                                                                              p_source_type                  => null,
                                                                              p_time_normal_finish           => null,
                                                                              p_time_normal_start            => null,
                                                                              p_bargaining_unit_code         => null,
                                                                              p_labour_union_member_flag     => null,
                                                                              p_hourly_salaried_code         => null,
                                                                              p_ass_attribute_category       => null,
                                                                              p_ass_attribute1               => null,
                                                                              p_ass_attribute2               => null,
                                                                              p_ass_attribute3               => null,
                                                                              p_ass_attribute4               => null,
                                                                              p_ass_attribute5               => null,
                                                                              p_ass_attribute6               => null,
                                                                              p_ass_attribute7               => null,
                                                                              p_ass_attribute8               => null,
                                                                              p_ass_attribute9               => null,
                                                                              p_ass_attribute10              => null,
                                                                              p_ass_attribute11              => null,
                                                                              p_ass_attribute12              => null,
                                                                              p_ass_attribute13              => null,
                                                                              p_ass_attribute14              => null,
                                                                              p_ass_attribute15              => null,
                                                                              p_ass_attribute16              => null,
                                                                              p_ass_attribute17              => null,
                                                                              p_ass_attribute18              => null,
                                                                              p_ass_attribute19              => null,
                                                                              p_ass_attribute20              => null,
                                                                              p_ass_attribute21              => null,
                                                                              p_ass_attribute22              => null,
                                                                              p_ass_attribute23              => null,
                                                                              p_ass_attribute24              => null,
                                                                              p_ass_attribute25              => null,
                                                                              p_ass_attribute26              => null,
                                                                              p_ass_attribute27              => null,
                                                                              p_ass_attribute28              => null,
                                                                              p_ass_attribute29              => null,
                                                                              p_ass_attribute30              => null,
                                                                              p_title                        => null,
                                                                              p_segment1                     => null,
                                                                              p_segment2                     => null,
                                                                              p_segment3                     => null,
                                                                              p_segment4                     => null,
                                                                              p_segment5                     => null,
                                                                              p_segment6                     => null,
                                                                              p_segment7                     => null,
                                                                              p_segment8                     => null,
                                                                              p_segment9                     => null,
                                                                              p_segment10                    => null,
                                                                              p_segment11                    => null,
                                                                              p_segment12                    => null,
                                                                              p_segment13                    => null,
                                                                              p_segment14                    => null,
                                                                              p_segment15                    => null,
                                                                              p_segment16                    => null,
                                                                              p_segment17                    => null,
                                                                              p_segment18                    => null,
                                                                              p_segment19                    => null,
                                                                              p_segment20                    => null,
                                                                              p_segment21                    => null,
                                                                              p_segment22                    => null,
                                                                              p_segment23                    => null,
                                                                              p_segment24                    => null,
                                                                              p_segment25                    => null,
                                                                              p_segment26                    => null,
                                                                              p_segment27                    => null,
                                                                              p_segment28                    => null,
                                                                              p_segment29                    => null,
                                                                              p_segment30                    => null,
                                                                              p_concat_segments              => null,*/
                                       p_supervisor_assignment_id => null
                                       --Out Parameters
                                      ,
                                       p_concatenated_segments  => lc_concatenated_segments,
                                       p_soft_coding_keyflex_id => lc_soft_coding_keyflex_id,
                                       p_comment_id             => ln_comment_id,
                                       p_effective_start_date   => ld_effective_start_date,
                                       p_effective_end_date     => ld_effective_end_date,
                                       p_no_managers_warning    => lb_no_managers_warning,
                                       p_other_manager_warning  => lb_other_manager_warning);
    
    END LOOP;
  END update_assignments;
  procedure emp_upd is
    /*
    THIS PROCEDURE WAS CREATED TO UPDATE EMPLOYEE EMAIL ADDRESS
    THROUGH API ON ADHOC BASIS (AZFAR  15 DEC 2017
    
    */
    -- Local Variables
    -- -----------------------
    ln_object_version_number NUMBER; --PER_ALL_PEOPLE_F.OBJECT_VERSION_NUMBER%TYPE  := 7;
    lc_dt_ud_mode            VARCHAR2(100) := NULL;
    ln_assignment_id         PER_ALL_ASSIGNMENTS_F.ASSIGNMENT_ID%TYPE; --    := 16384;
    lc_employee_number       PER_ALL_PEOPLE_F.EMPLOYEE_NUMBER%TYPE; --    := '555440';
    lc_person_id             number;
    lc_email                 VARCHAR2(50);
    lc_effective_dt          DATE;
  
    lc_error_flag number;
    lc_error_msg  varchar2(200);
    lv_error_msg  varchar2(500);
    -- Out Variables for Find Date Track Mode API
    -- ----------------------------------------------------------------
    lb_correction           BOOLEAN;
    lb_update               BOOLEAN;
    lb_update_override      BOOLEAN;
    lb_update_change_insert BOOLEAN;
  
    -- Out Variables for Update Employee API
    -- -----------------------------------------------------------
    ld_effective_start_date     DATE;
    ld_effective_end_date       DATE;
    lc_full_name                PER_ALL_PEOPLE_F.FULL_NAME%TYPE;
    ln_comment_id               PER_ALL_PEOPLE_F.COMMENT_ID%TYPE;
    lb_name_combination_warning BOOLEAN;
    lb_assign_payroll_warning   BOOLEAN;
    lb_orig_hire_warning        BOOLEAN;
  
  BEGIN
  
    --lc_email:='fahad.hamid2@hbl.com';
    --lc_employee_number:='586987';
    lc_error_flag := 0;
  
    for i in (select a.record_id, a.employee_number, a.email_address
                from XXHBL_EMPLOYEE_STG a
               where 1 = 1
                 and a.status is null
                 and a.record_id >= 65000) loop
    
      Begin
        lc_person_id := NULL;
        SELECT PERSON_ID, OBJECT_VERSION_NUMBER, EFFECTIVE_START_DATE
          INTO lc_person_id, ln_object_version_number, lc_effective_dt
          FROM PER_ALL_PEOPLE_F PAPF
         WHERE PERSON_TYPE_ID IN
               (SELECT distinct b.person_type_id
                  FROM APPS.PER_PERSON_TYPES_V    B,
                       APPS.HR_ORGANIZATION_UNITS C
                --XXHBL_HCM_INTEG_MAPPING    D
                 WHERE B.business_group_id = C.business_group_id
                      --  and c.business_group_id = d.business_group_id
                   AND B.system_name = 'Employee'
                   AND C.type = 'BG'
                --1126
                )
           AND PAPF.EMPLOYEE_NUMBER = i.employee_number --lc_employee_number
           AND TRUNC(SYSDATE) BETWEEN PAPF.EFFECTIVE_START_DATE AND
               PAPF.EFFECTIVE_END_DATE;
      Exception
        When others THEN
          lc_error_flag := 1;
          lc_error_msg  := sqlerrm;
        
      END;
      /*
       -- Find Date Track Mode
       -- --------------------------------
       dt_api.find_dt_upd_modes
        (    -- Input Data Elements
             -- ------------------------------
             p_effective_date                    => TO_DATE('08-OCT-2016'),
             p_base_table_name                   => 'PER_ALL_ASSIGNMENTS_F',
             p_base_key_column                   => 'ASSIGNMENT_ID',
             p_base_key_value                    => ln_assignment_id,
             -- Output data elements
             -- -------------------------------
            p_correction                                   => lb_correction,
            p_update                                         => lb_update,
            p_update_override                       => lb_update_override,
            p_update_change_insert            => lb_update_change_insert
      );
      
      IF ( lb_update_override = TRUE OR lb_update_change_insert = TRUE )
      THEN
             -- UPDATE_OVERRIDE
             -- ---------------------------------
             lc_dt_ud_mode := 'UPDATE_OVERRIDE';
      END IF;
      
      IF ( lb_correction = TRUE )
      THEN
            -- CORRECTION
            -- ----------------------
            lc_dt_ud_mode := 'CORRECTION';
      END IF;
      
      IF ( lb_update = TRUE )
      THEN
           -- UPDATE
           -- --------------
            lc_dt_ud_mode := 'UPDATE';
      END IF;
      */
    
      lc_dt_ud_mode := 'CORRECTION';
    
      -- Update Employee API
      if lc_error_flag = 0 then
        -- ---------------------------------
        begin
        
          hr_person_api.update_person( -- Input Data Elements
                                      -- ------------------------------
                                      p_effective_date        => lc_effective_dt, --TO_DATE('08-OCT-2016'),
                                      p_datetrack_update_mode => lc_dt_ud_mode,
                                      p_person_id             => lc_person_id,
                                      --p_middle_names                => 'TEST',
                                      --p_marital_status              => 'M',
                                      P_EMAIL_ADDRESS => i.email_address, --lc_email,
                                      -- Output Data Elements
                                      -- ----------------------------------
                                      p_employee_number          => i.employee_number, --lc_employee_number,
                                      p_object_version_number    => ln_object_version_number,
                                      p_effective_start_date     => ld_effective_start_date,
                                      p_effective_end_date       => ld_effective_end_date,
                                      p_full_name                => lc_full_name,
                                      p_comment_id               => ln_comment_id,
                                      p_name_combination_warning => lb_name_combination_warning,
                                      p_assign_payroll_warning   => lb_assign_payroll_warning,
                                      p_orig_hire_warning        => lb_orig_hire_warning);
        
          COMMIT;
        
          update XXHBL_EMPLOYEE_STG jj
             set jj.status    = 'S',
                 jj.error_msg = 'email updated successfull'
           where jj.record_id = i.record_id;
          --dbms_output.put_line('success');
        exception
          when others then
          
            lv_error_msg := sqlerrm;
          
            update XXHBL_EMPLOYEE_STG jj
               set jj.status    = 'E',
                   jj.error_msg = 'API error ' || lv_error_msg
             where jj.record_id = i.record_id;
          
          --dbms_output.put_line('fail');
        end;
      
      else
      
        update XXHBL_EMPLOYEE_STG jj
           set jj.status = 'E', jj.error_msg = 'Check employee rec'
         where jj.record_id = i.record_id;
      
        lc_error_flag := 0;
      
        --dbms_output.put_line(lc_error_msg);
      end if;
    
    end loop;
  
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      dbms_output.put_line(SQLERRM);
  end emp_upd;

  Procedure emp_pay_method_upd is
  
    /*
    THIS PROCEDURE WAS CREATED TO UPDATE EMPLOYEE PAYMENT METHOD
    THROUGH API ON ADHOC BASIS (AZFAR  15 DEC 2017)
    THIS WILL USE A STAGE TABLE
    
        create table XXX_PAYMENT_METHOD
      (
        rec_id          NUMBER,
        effective_date  DATE,
        employee_number VARCHAR2(10),
        method_name     VARCHAR2(50),
        account_title   VARCHAR2(50),
        account_num     VARCHAR2(50),
        account_type    VARCHAR2(10),
        bank_code       VARCHAR2(10),
        bank_name       VARCHAR2(50),
        branch_code     VARCHAR2(10),
        branch_name     VARCHAR2(50),
        profit          VARCHAR2(10),
        status_flag     VARCHAR2(10),
        status_desc     VARCHAR2(200)
      )
    
    */
  
    CURSOR get_details IS
      SELECT paaf.assignment_id,
             per.EMPLOYEE_NUMBER,
             paaf.effective_start_date,
             dept.EFFECTIVE_DATE,
             dept.rec_id,
             dept.branch_code || dept.account_num account_num,
             per.attribute1,
             dept.bank_code,
             dept.branch_code,
             pppmf.personal_payment_method_id,
             pppmf.object_version_number
        FROM per_all_people_f                   per,
             per_all_assignments_f              paaf,
             hbl_integ_stage.XXX_payment_method dept,
             pay_personal_payment_methods_f     pppmf
       WHERE per.person_id = paaf.person_id
         AND per.person_type_id IN (1126, 1127)
         AND paaf.assignment_type = 'E'
         AND paaf.primary_flag = 'Y'
         AND PER.EMPLOYEE_NUMBER = DEPT.EMPLOYEE_NUMBER
         AND paaf.assignment_status_type_id IN (1, 2)
         AND SYSDATE BETWEEN per.effective_start_date AND
             per.effective_end_date
         AND SYSDATE BETWEEN paaf.effective_start_date AND
             paaf.effective_end_date
         AND SYSDATE BETWEEN pppmf.EFFECTIVE_START_DATE AND
             pppmf.EFFECTIVE_END_DATE
            --AND dept.legacy_employee_number = per.attribute1
         AND pppmf.assignment_id = paaf.assignment_id
         AND depT.Status_Flag is null
      --and dept.rec_id=2
      ;
  
    l_effective_start_date       DATE := NULL;
    l_effective_end_date         DATE := NULL;
    l_object_version_number      NUMBER := NULL;
    l_external_account_id        NUMBER := NULL;
    l_org_payment_method_id      NUMBER := NULL;
    l_assignment_id              NUMBER := NULL;
    l_personal_payment_method_id NUMBER := NULL;
    l_comment_id                 NUMBER := NULL;
    l_bank_code                  VARCHAR2(50) := NULL;
    l_err_msg                    VARCHAR2(500) := NULL;
    l_obj_ver                    number;
  BEGIN
    --Initialize Session
    fnd_global. apps_initialize(user_id      => 1131, -- XXX_MIGRATION
                                resp_id      => 21514, -- XXX HRMS Manager
                                resp_appl_id => 800 -- Human Resouces
                                );
  
    -- Get Organization Payment ID
    BEGIN
      SELECT popm.org_payment_method_id
        INTO l_org_payment_method_id
        FROM pay_org_payment_methods_f popm
       WHERE SYSDATE BETWEEN popm.effective_start_date AND
             popm.effective_end_date
         AND org_payment_method_name = 'HBL Payment Method';
    EXCEPTION
      WHEN OTHERS THEN
        l_err_msg               := 'l_org_payment_id  not found !!!';
        l_org_payment_method_id := NULL;
    END;
  
    FOR fetch_details IN get_details LOOP
      l_err_msg                    := NULL;
      l_personal_payment_method_id := NULL;
      l_external_account_id        := NULL;
      l_object_version_number      := NULL;
      l_effective_start_date       := NULL;
      l_effective_end_date         := NULL;
      l_comment_id                 := NULL;
    
      -- Get Employee Bank Code
      /*      BEGIN
         SELECT hl.lookup_code
           INTO l_bank_code
           FROM hr_lookups hl
          WHERE hl.lookup_type = 'AE_BANK_NAMES'
                AND UPPER (hl.meaning) = UPPER (fetch_details.bank);
      EXCEPTION
         WHEN OTHERS
         THEN
            l_err_msg := l_err_msg || ' -  ' || ' l_bank_code  not found !!!';
            l_bank_code := NULL;
      END;*/
    
      IF l_org_payment_method_id IS NOT NULL --AND l_bank_code IS NOT NULL
       THEN
        BEGIN
          hr_personal_pay_method_api. update_personal_pay_method(p_validate                   => FALSE,
                                                                 p_effective_date             => fetch_details.effective_date, --  TO_DATE('01-FEB-2018'),
                                                                 p_datetrack_update_mode      => 'UPDATE',
                                                                 p_personal_payment_method_id => fetch_details.personal_payment_method_id, --l_org_payment_method_id, --fetch_details.personal_payment_method_id,
                                                                 p_object_version_number      => fetch_details.object_version_number,
                                                                 p_percentage                 => 100,
                                                                 p_priority                   => 99,
                                                                 p_territory_code             => 'PK',
                                                                 p_segment2                   => fetch_details.account_num,
                                                                 p_segment3                   => 'Saving',
                                                                 p_segment4                   => '001',
                                                                 p_segment5                   => fetch_details.branch_code,
                                                                 p_comment_id                 => l_comment_id,
                                                                 p_external_account_id        => l_external_account_id,
                                                                 p_effective_start_date       => l_effective_start_date,
                                                                 p_effective_end_date         => l_effective_end_date);
        
          --COMMIT;
        
          /*   DBMS_OUTPUT.
          put_line (
            l_personal_payment_method_id
            || ' has been Created Successfully');*/
        
          update hbl_integ_stage.XXX_payment_method xx
             set xx.status_flag = 'S'
           where xx.rec_id = fetch_details.rec_id;
        
        EXCEPTION
          WHEN OTHERS THEN
            l_err_msg := SQLERRM;
            /*
            DBMS_OUTPUT.
             put_line (
                  'Innner Exception: '
               --|| fetch_details.assignment_id
               || ' - '
               || l_err_msg);*/
          
            update hbl_integ_stage.XXX_payment_method xx
               set xx.status_flag = 'E',
                   xx.status_desc = substr(l_err_msg, 1, 190)
             where xx.rec_id = fetch_details.rec_id;
        END;
      ELSE
        DBMS_OUTPUT.put_line('Org Payment or Bank Code not Found !!!');
      
      END IF;
    END LOOP;
  
    COMMIT;
  EXCEPTION
    WHEN OTHERS THEN
      l_err_msg := SQLERRM;
      DBMS_OUTPUT.put_line('Main Exception: ' || l_err_msg);
    
  end emp_pay_method_upd;

  Procedure emp_pay_method_create is
    ln_method_id      PAY_PERSONAL_PAYMENT_METHODS_F.PERSONAL_PAYMENT_METHOD_ID%TYPE;
    ln_ext_acc_id     PAY_EXTERNAL_ACCOUNTS.EXTERNAL_ACCOUNT_ID%TYPE;
    ln_obj_ver_num    PAY_PERSONAL_PAYMENT_METHODS_F.OBJECT_VERSION_NUMBER%TYPE;
    ld_eff_start_date DATE;
    ld_eff_end_date   DATE;
    ln_comment_id     NUMBER;
  
  BEGIN
    -- Create Employee Payment Method
    -- --------------------------------------------------
    hr_personal_pay_method_api.create_personal_pay_method( -- Input data elements
                                                          -- ------------------------------
                                                          p_effective_date        => TO_DATE('15-DEC-2017'),
                                                          p_assignment_id         => 16967,
                                                          p_org_payment_method_id => 61,
                                                          p_priority              => 99,
                                                          p_percentage            => 100,
                                                          p_territory_code        => 'PK',
                                                          --p_segment1                            => 'PRAJKUMAR',
                                                          p_segment2 => '11027900647901', --ACNO
                                                          p_segment3 => 'Saving',
                                                          p_segment4 => '001',
                                                          p_segment5 => '1102', --BR
                                                          -- p_segment6                            => 'INDIA',
                                                          -- Output data elements
                                                          -- --------------------------------
                                                          p_personal_payment_method_id => ln_method_id,
                                                          p_external_account_id        => ln_ext_acc_id,
                                                          p_object_version_number      => ln_obj_ver_num,
                                                          p_effective_start_date       => ld_eff_start_date,
                                                          p_effective_end_date         => ld_eff_end_date,
                                                          p_comment_id                 => ln_comment_id);
  
    COMMIT;
  
  EXCEPTION
    WHEN OTHERS THEN
      ROLLBACK;
      dbms_output.put_line(SQLERRM);
    
  end emp_pay_method_create;

  Procedure emp_costing_create is
  
    CURSOR C1 IS
      SELECT DISTINCT XX.EMPLOYEE_NUMBER, XX.ACC_CODE, PASF.ASSIGNMENT_ID
        FROM XX_COSTING_ACC        XX,
             PER_ALL_PEOPLE_F      PAPF,
             PER_ALL_ASSIGNMENTS_F PASF
       WHERE XX.EMPLOYEE_NUMBER = PAPF.EMPLOYEE_NUMBER
         AND PAPF.PERSON_ID = PASF.PERSON_ID
         AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE AND
             PAPF.EFFECTIVE_END_DATE
         AND SYSDATE BETWEEN PASF.EFFECTIVE_START_DATE AND
             PASF.EFFECTIVE_END_DATE
         AND PASF.BUSINESS_GROUP_ID = 81
         AND EXISTS (SELECT '1'
                FROM XX_COSTING_ACC XCA
               WHERE XCA.EMPLOYEE_NUMBER = XX.EMPLOYEE_NUMBER)
         AND NVL(STATUS, 'N') = 'N';
    --AND XX.EMPLOYEE_NUMBER='618410';
  
    L_PROPORTION                 PAY_COST_ALLOCATIONS_F.PROPORTION%TYPE := 1;
    L_BUSINESS_GROUP_ID          NUMBER := 81;
    L_COMBINATION_NAME           VARCHAR2(100);
    L_COST_ALLOCATION_ID         NUMBER;
    L_COST_EFFECTIVE_START_DATE  DATE;
    L_COST_EFFECTIVE_END_DATE    DATE;
    L_COST_OBJ_VERSION_NUMBER    NUMBER;
    L_COST_ALLOCATION_KEYFLEX_ID PAY_COST_ALLOCATION_KEYFLEX.COST_ALLOCATION_KEYFLEX_ID%TYPE;
  
  BEGIN
  
    FOR I IN C1 LOOP
    
      L_COST_ALLOCATION_KEYFLEX_ID := NULL;
    
      BEGIN
        PAY_COST_ALLOCATION_API.CREATE_COST_ALLOCATION(P_VALIDATE                   => FALSE,
                                                       P_EFFECTIVE_DATE             => '01-JAN-2018',
                                                       P_ASSIGNMENT_ID              => I.ASSIGNMENT_ID,
                                                       P_PROPORTION                 => L_PROPORTION,
                                                       P_BUSINESS_GROUP_ID          => L_BUSINESS_GROUP_ID,
                                                       P_SEGMENT2                   => I.ACC_CODE,
                                                       P_COMBINATION_NAME           => L_COMBINATION_NAME,
                                                       P_COST_ALLOCATION_ID         => L_COST_ALLOCATION_ID,
                                                       P_EFFECTIVE_START_DATE       => L_COST_EFFECTIVE_START_DATE,
                                                       P_EFFECTIVE_END_DATE         => L_COST_EFFECTIVE_END_DATE,
                                                       P_OBJECT_VERSION_NUMBER      => L_COST_OBJ_VERSION_NUMBER,
                                                       P_COST_ALLOCATION_KEYFLEX_ID => L_COST_ALLOCATION_KEYFLEX_ID);
      
        IF L_COST_OBJ_VERSION_NUMBER IS NULL THEN
          UPDATE XX_COSTING_ACC
             SET STATUS = 'F'
           WHERE EMPLOYEE_NUMBER = I.EMPLOYEE_NUMBER;
          ROLLBACK;
        ELSE
          UPDATE XX_COSTING_ACC
             SET STATUS = 'S'
           WHERE EMPLOYEE_NUMBER = I.EMPLOYEE_NUMBER;
        
          COMMIT;
        END IF;
      
      EXCEPTION
        WHEN OTHERS THEN
          --DBMS_OUTPUT.PUT_LINE('Cost Allocation creation failed-->'||SQLERRM);
          UPDATE XX_COSTING_ACC
             SET STATUS = 'E'
           WHERE EMPLOYEE_NUMBER = I.EMPLOYEE_NUMBER;
      END;
    
    END LOOP;
  
  end emp_costing_create;

  Procedure emp_costing_delete is
  
    CURSOR C1 IS
      SELECT PAPF.EMPLOYEE_NUMBER,
             PAPF.FULL_NAME,
             PASF.ASSIGNMENT_ID,
             PCAF.OBJECT_VERSION_NUMBER,
             PCAF.COST_ALLOCATION_ID,
             PCAK.SEGMENT2
        FROM PER_ALL_PEOPLE_F            PAPF,
             PER_ALL_ASSIGNMENTS_F       PASF,
             PAY_COST_ALLOCATIONS_F      PCAF,
             PAY_COST_ALLOCATION_KEYFLEX PCAK
       WHERE PAPF.BUSINESS_GROUP_ID = 81
         AND PAPF.PERSON_ID = PASF.PERSON_ID
         AND SYSDATE BETWEEN PAPF.EFFECTIVE_START_DATE AND
             PAPF.EFFECTIVE_END_DATE
         AND SYSDATE BETWEEN PASF.EFFECTIVE_START_DATE AND
             PASF.EFFECTIVE_END_DATE
         AND PASF.ASSIGNMENT_ID = PCAF.ASSIGNMENT_ID(+)
         AND SYSDATE BETWEEN NVL(PCAF.EFFECTIVE_START_DATE, SYSDATE) AND
             NVL(PCAF.EFFECTIVE_END_DATE, SYSDATE)
         AND PCAF.COST_ALLOCATION_KEYFLEX_ID =
             PCAK.COST_ALLOCATION_KEYFLEX_ID(+)
         AND PCAK.SEGMENT2 IS NOT NULL
         AND PCAF.EFFECTIVE_END_DATE = '31-DEC-4712'
         AND EMPLOYEE_NUMBER IN (SELECT EMPLOYEE_NUMBER
                                   FROM XX_COSTING_ACC
                                  WHERE STATUS = 'S' --NVL(STATUS,'X') != 'X'
                                 );
  
    L_COST_EFFECTIVE_START_DATE DATE;
    L_COST_EFFECTIVE_END_DATE   DATE;
  
  BEGIN
  
    FOR I IN C1 LOOP
    
      BEGIN
        PAY_COST_ALLOCATION_API.DELETE_COST_ALLOCATION(P_EFFECTIVE_DATE        => '31-MAR-2018',
                                                       P_DATETRACK_DELETE_MODE => 'DELETE',
                                                       P_COST_ALLOCATION_ID    => I.COST_ALLOCATION_ID,
                                                       P_OBJECT_VERSION_NUMBER => I.OBJECT_VERSION_NUMBER,
                                                       P_EFFECTIVE_START_DATE  => L_COST_EFFECTIVE_START_DATE,
                                                       P_EFFECTIVE_END_DATE    => L_COST_EFFECTIVE_END_DATE);
      
        --DBMS_OUTPUT.PUT_LINE('Sucess    '||I.EMPLOYEE_NUMBER);
      
      EXCEPTION
        WHEN OTHERS THEN
          DBMS_OUTPUT.PUT_LINE('Failure   ' || I.EMPLOYEE_NUMBER || '  ' ||
                               SQLERRM);
      END;
    END LOOP;
  
    COMMIT;
  
  end emp_costing_delete;

  PROCEDURE emp_fnduser_email_upd is
  
    lc_user_name VARCHAR2(100); --   := '586987';
    --lc_user_password                   VARCHAR2(100)   := 'Oracle123';
    --ld_user_start_date                 DATE            := TO_DATE('23-JUN-2012');
    --ld_user_end_date                   VARCHAR2(100)   := NULL;
    --ld_password_date                   VARCHAR2(100)   := TO_DATE('23-JUN-2012');
    --ld_password_lifespan_days          NUMBER          := 90;
    lc_person_id             NUMBER; -- := 71250;
    lc_email_address         VARCHAR2(100); -- := 'fahad@hbl.com';
    lv_error_msg             varchar2(100);
    ln_object_version_number number;
    lc_effective_dt          date;
    lc_error_flag            number;
    lc_error_msg             varchar2(100);
  
  BEGIN
  
    for i in (select a.record_id, a.employee_number, a.email_address
                from XXHBL_EMPLOYEE_STG a
               where a.status is null
                 and a.record_id > 62000) loop
    
      Begin
        lc_person_id  := NULL;
        lc_error_flag := 0;
      
        SELECT PERSON_ID,
               OBJECT_VERSION_NUMBER,
               EFFECTIVE_START_DATE,
               FU.USER_NAME
          INTO lc_person_id,
               ln_object_version_number,
               lc_effective_dt,
               lc_user_name
          FROM PER_ALL_PEOPLE_F PAPF, FND_USER FU
         WHERE PAPF.PERSON_ID = FU.EMPLOYEE_ID
              -- PERSON_TYPE_ID IN (1126)
           AND PAPF.EMPLOYEE_NUMBER = i.employee_number --lc_employee_number
           AND TRUNC(SYSDATE) BETWEEN PAPF.EFFECTIVE_START_DATE AND
               PAPF.EFFECTIVE_END_DATE;
      
      Exception
        When others THEN
          lc_error_flag := 1;
          lc_error_msg  := sqlerrm;
        
      END;
    
      if lc_error_flag = 0 then
        begin
          fnd_user_pkg.updateuser(x_user_name => lc_user_name,
                                  x_owner     => NULL,
                                  --x_unencrypted_password    => lc_user_password,
                                  -- x_start_date              => ld_user_start_date,
                                  --x_end_date                => ld_user_end_date,
                                  -- x_password_date           => ld_password_date,
                                  --x_password_lifespan_days  => ld_password_lifespan_days,
                                  x_employee_id   => lc_person_id, --ln_person_id,
                                  x_email_address => I.EMAIL_ADDRESS);
        
          update XXHBL_EMPLOYEE_STG jj
             set jj.status    = 'S',
                 jj.error_msg = 'email updated successfull'
           where jj.record_id = i.record_id;
          --dbms_output.put_line('success');
        
        exception
          WHEN OTHERS THEN
            lv_error_msg := sqlerrm;
          
            update XXHBL_EMPLOYEE_STG jj
               set jj.status    = 'E',
                   jj.error_msg = 'API Exception ' || lv_error_msg
             where jj.record_id = i.record_id;
            dbms_output.put_line('fail ' || i.employee_number);
          
        end;
      
      else
        update XXHBL_EMPLOYEE_STG jj
           set jj.status = 'E', jj.error_msg = 'Check employee'
         where jj.record_id = i.record_id;
        dbms_output.put_line('fail ' || i.employee_number);
      end if;
    
    end loop;
    --commit;
  
  END emp_fnduser_email_upd;
  
  procedure Update_Position_overlap(p_position_id in number default null,
                                    p_ov          in number default null) is
    --(lv_posid in number)
    l_true                         BOOLEAN := FALSE;
    l_obj                          NUMBER;
    l_DATE_EFFECTIVE               DATE; --           :=to_date('01-Jan-2016','DD-MON-YYYY');
    l_POSITION_ID                  NUMBER; --         :=9214;
    l_POSITION_DEFINITION_ID       NUMBER;
    l_NAME                         VARCHAR2(500); --  :='0000741-Senior Team Leader';
    l_position_type                VARCHAR2(100); --  := 'SINGLE';
    L_EFFECTIVE_DATE_START         DATE;
    L_EFFECTIVE_DATE_END           DATE;
    L_VALID_GRADES_CHANGED_WARNING BOOLEAN;
    l_datetrack_mode               VARCHAR2(100) := 'CORRECTION';
    l_stat                         char := 'S';
    l_msg                          varchar2(1000);
    /*CURSOR csr_ovn
    IS
      SELECT MAX(object_version_number)
      FROM hr_all_positions_f
      WHERE position_id=l_POSITION_ID;*/
  
  BEGIN
    /*OPEN csr_ovn;
    FETCH csr_ovn INTO l_obj;
    CLOSE csr_ovn;*/
  
    FOR I IN (select a.POSITION_ID,
                     a.POSITION_DEFINITION_ID,
                     a.JOB_ID,
                     a.LOCATION_ID,
                     a.ORGANIZATION_ID,
                     a.NAME,
                     a.DATE_EFFECTIVE,
                     a.PROBATION_PERIOD,
                     a.PROBATION_PERIOD_UNIT_CD,
                     a.POSITION_TYPE,
                     a.EFFECTIVE_START_DATE,
                     a.EFFECTIVE_END_DATE,
                     e.POSITION_EXTRA_INFO_ID,
                     e.OBJECT_VERSION_NUMBER
                from hr_all_positions_f a, PER_POSITION_EXTRA_INFO e
               where a.POSITION_ID = e.POSITION_ID(+)
                 and trunc(sysdate) between a.EFFECTIVE_START_DATE and
                     a.EFFECTIVE_END_DATE
                 and a.position_id = nvl(p_position_id, a.POSITION_ID)
                 and a.OVERLAP_PERIOD is not null
                 and exists
               (select ''
                        from hbl_integ_stage.xxhbl_position_del_overlap q
                       where q.status_flag is null
                         and q.position_id = a.POSITION_ID)
              
              ) loop
    
      if i.position_extra_info_id is not null then
        begin
          hr_position_extra_info_api.delete_position_extra_info(p_validate               => l_true,
                                                                p_position_extra_info_id => i.position_extra_info_id,
                                                                p_object_version_number  => i.object_version_number);
          l_stat := 'S';
        exception
          when others then
            update hbl_integ_stage.xxhbl_position_del_overlap q
               set q.status_flag = 'E',
                   Q.ERROR_MSG   = 'Error in deleting overlapping dates'
             where q.position_id = i.position_id;
          
            l_stat := 'E';
            l_msg  := 'Error in deleting overlapping dates';
          
        end;
      end if;
    
      if l_stat != 'E' then
      
        if p_ov is null then
          SELECT min(object_version_number)
            into l_obj
            FROM hr_all_positions_f
           WHERE position_id = i.position_id;
          --AND Trunc(sysdate) between effective_start_date and effective_end_date;
        else
          l_obj := p_ov;
        end if;
      
        begin
        
          HR_POSITION_API.UPDATE_POSITION(p_validate                     => l_true,
                                          p_position_id                  => i.position_id --l_POSITION_ID
                                         ,
                                          p_effective_start_date         => l_EFFECTIVE_DATE_START,
                                          p_effective_end_date           => l_EFFECTIVE_DATE_END,
                                          p_position_definition_id       => i.position_definition_id --l_POSITION_DEFINITION_ID
                                         ,
                                          p_valid_grades_changed_warning => L_VALID_GRADES_CHANGED_WARNING,
                                          p_name                         => i.name,
                                          --p_probation_period         => NULL,
                                          --p_probation_period_unit_cd => NULL,
                                          p_overlap_period        => null,
                                          p_overlap_unit_cd       => null,
                                          p_object_version_number => l_obj,
                                          p_effective_date        => to_date(i.effective_start_date) --to_date(i.date_effective) --,'DD-MON-YYYY')
                                         ,
                                          p_datetrack_mode        => l_datetrack_mode
                                          
                                          );
        
          update hbl_integ_stage.xxhbl_position_del_overlap q
             set q.status_flag = 'P'
           where q.position_id = i.position_id;
        
          l_stat := 'S';
        exception
          when others then
          
            l_stat := 'E';
            l_msg  := sqlerrm;
          
            update hbl_integ_stage.xxhbl_position_del_overlap q
               set q.status_flag = 'E', Q.ERROR_MSG = l_msg
             where q.position_id = i.position_id;
          
          /*dbms_output.put_line('API Exception -' || i.position_id || '-' ||
          sqlerrm);*/
        end;
      end if;
    end loop;
  END;

  
  procedure Update_Position_budgetflag(p_position_id in number default null,
                                    p_ov          in number default null) is
    --(lv_posid in number)
    l_true                         BOOLEAN := FALSE;
    l_obj                          NUMBER;
    l_DATE_EFFECTIVE               DATE; --           :=to_date('01-Jan-2016','DD-MON-YYYY');
    l_POSITION_ID                  NUMBER; --         :=9214;
    l_POSITION_DEFINITION_ID       NUMBER;
    l_NAME                         VARCHAR2(500); --  :='0000741-Senior Team Leader';
    l_position_type                VARCHAR2(100); --  := 'SINGLE';
    L_EFFECTIVE_DATE_START         DATE;
    L_EFFECTIVE_DATE_END           DATE;
    L_VALID_GRADES_CHANGED_WARNING BOOLEAN;
    l_datetrack_mode               VARCHAR2(100) := 'CORRECTION';
    l_stat                         char := 'S';
    l_msg                          varchar2(1000);
    /*CURSOR csr_ovn
    IS
      SELECT MAX(object_version_number)
      FROM hr_all_positions_f
      WHERE position_id=l_POSITION_ID;*/
  
  BEGIN
    /*OPEN csr_ovn;
    FETCH csr_ovn INTO l_obj;
    CLOSE csr_ovn;*/
  
    FOR I IN (select a.POSITION_ID,
                     a.POSITION_DEFINITION_ID,
                     a.JOB_ID,
                     a.LOCATION_ID,
                     a.ORGANIZATION_ID,
                     a.NAME,
                     a.DATE_EFFECTIVE,
                     a.PROBATION_PERIOD,
                     a.PROBATION_PERIOD_UNIT_CD,
                     a.POSITION_TYPE,
                     a.EFFECTIVE_START_DATE,
                     a.EFFECTIVE_END_DATE,
                     e.POSITION_EXTRA_INFO_ID,
                     e.OBJECT_VERSION_NUMBER
                from hr_all_positions_f a, PER_POSITION_EXTRA_INFO e
               where a.POSITION_ID = e.POSITION_ID(+)
                 and trunc(sysdate) between a.EFFECTIVE_START_DATE and
                     a.EFFECTIVE_END_DATE
                 and a.position_id = nvl(p_position_id, a.POSITION_ID)
                 --and a.OVERLAP_PERIOD is not null
                 and exists
               (select ''
                        from hbl_integ_stage.xxhbl_position_del_overlap q
                       where q.status_flag is null
                         and q.position_id = a.POSITION_ID)
              
              ) loop
    
      if i.position_extra_info_id is not null then
        begin
          hr_position_extra_info_api.delete_position_extra_info(p_validate               => l_true,
                                                                p_position_extra_info_id => i.position_extra_info_id,
                                                                p_object_version_number  => i.object_version_number);
          l_stat := 'S';
        exception
          when others then
            update hbl_integ_stage.xxhbl_position_del_overlap q
               set q.status_flag = 'E',
                   Q.ERROR_MSG   = 'Error in deleting overlapping dates'
             where q.position_id = i.position_id;
          
            l_stat := 'E';
            l_msg  := 'Error in deleting overlapping dates';
          
        end;
      end if;
    
      if l_stat != 'E' then
      
        if p_ov is null then
          SELECT min(object_version_number)
            into l_obj
            FROM hr_all_positions_f
           WHERE position_id = i.position_id;
          --AND Trunc(sysdate) between effective_start_date and effective_end_date;
        else
          l_obj := p_ov;
        end if;
      
        begin
        
          HR_POSITION_API.UPDATE_POSITION(p_validate                     => l_true,
                                          p_position_id                  => i.position_id --l_POSITION_ID
                                         ,
                                          p_effective_start_date         => l_EFFECTIVE_DATE_START,
                                          p_effective_end_date           => l_EFFECTIVE_DATE_END,
                                          p_position_definition_id       => i.position_definition_id --l_POSITION_DEFINITION_ID
                                         ,
                                          p_valid_grades_changed_warning => L_VALID_GRADES_CHANGED_WARNING,
                                          p_name                         => i.name,
                                          --p_probation_period         => NULL,
                                          --p_probation_period_unit_cd => NULL,
                                          --p_overlap_period        => null,
                                          --p_overlap_unit_cd       => null,
                                          p_attribute3 => 'Yes',
                                          p_object_version_number => l_obj,
                                          p_effective_date        => to_date(i.date_effective) --,'DD-MON-YYYY')
                                         ,
                                          p_datetrack_mode        => l_datetrack_mode
                                          
                                          );
        
          update hbl_integ_stage.xxhbl_position_del_overlap q
             set q.status_flag = 'P'
           where q.position_id = i.position_id;
        
          l_stat := 'S';
        exception
          when others then
          
            l_stat := 'E';
            l_msg  := sqlerrm;
          
            update hbl_integ_stage.xxhbl_position_del_overlap q
               set q.status_flag = 'E', Q.ERROR_MSG = l_msg
             where q.position_id = i.position_id;
          
          /*dbms_output.put_line('API Exception -' || i.position_id || '-' ||
          sqlerrm);*/
        end;
      end if;
    end loop;
  END;

  PROCEDURE IBT_EMPLOYEE_API(p_validate                     in boolean default false,
                             p_hire_date                    in date,
                             p_business_group_id            in number,
                             p_last_name                    in varchar2,
                             p_sex                          in varchar2,
                             p_person_type_name             in varchar2 default null,
                             p_nationality_name             in varchar2 default null,
                             p_marital_status               in varchar2 default null,
                             p_title                        in varchar2 default null,
                             p_country_of_birth             in varchar2 default null,
                             p_per_comments                 in varchar2 default null,
                             p_date_employee_data_verified  in date default null,
                             p_date_of_birth                in date default null,
                             p_email_address                in varchar2 default null,
                             p_employee_number              in out nocopy varchar2,
                             p_expense_check_send_to_addres in varchar2 default null,
                             p_first_name                   in varchar2 default null,
                             p_known_as                     in varchar2 default null,
                             p_middle_names                 in varchar2 default null,
                             p_national_identifier          in varchar2 default null,
                             p_previous_last_name           in varchar2 default null,
                             p_registered_disabled_flag     in varchar2 default null,
                             p_vendor_id                    in number default null,
                             p_work_telephone               in varchar2 default null,
                             p_attribute_category           in varchar2 default null,
                             p_attribute1                   in varchar2 default null,
                             p_attribute2                   in varchar2 default null,
                             p_attribute3                   in varchar2 default null,
                             p_attribute4                   in varchar2 default null,
                             p_attribute5                   in varchar2 default null,
                             p_attribute6                   in varchar2 default null,
                             p_attribute7                   in varchar2 default null,
                             p_attribute8                   in varchar2 default null,
                             p_attribute9                   in varchar2 default null,
                             p_attribute10                  in varchar2 default null,
                             p_attribute11                  in varchar2 default null,
                             p_attribute12                  in varchar2 default null,
                             p_attribute13                  in varchar2 default null,
                             p_attribute14                  in varchar2 default null,
                             p_attribute15                  in varchar2 default null,
                             p_attribute16                  in varchar2 default null,
                             p_attribute17                  in varchar2 default null,
                             p_attribute18                  in varchar2 default null,
                             p_attribute19                  in varchar2 default null,
                             p_attribute20                  in varchar2 default null,
                             p_attribute21                  in varchar2 default null,
                             p_attribute22                  in varchar2 default null,
                             p_attribute23                  in varchar2 default null,
                             p_attribute24                  in varchar2 default null,
                             p_attribute25                  in varchar2 default null,
                             p_attribute26                  in varchar2 default null,
                             p_attribute27                  in varchar2 default null,
                             p_attribute28                  in varchar2 default null,
                             p_attribute29                  in varchar2 default null,
                             p_attribute30                  in varchar2 default null,
                             p_per_information_category     in varchar2 default null,
                             p_per_information1             in varchar2 default null,
                             p_per_information2             in varchar2 default null,
                             p_per_information3             in varchar2 default null,
                             p_per_information4             in varchar2 default null,
                             p_per_information5             in varchar2 default null,
                             p_per_information6             in varchar2 default null,
                             p_per_information7             in varchar2 default null,
                             p_per_information8             in varchar2 default null,
                             p_per_information9             in varchar2 default null,
                             p_per_information10            in varchar2 default null,
                             p_per_information11            in varchar2 default null,
                             p_per_information12            in varchar2 default null,
                             p_per_information13            in varchar2 default null,
                             p_per_information14            in varchar2 default null,
                             p_per_information15            in varchar2 default null,
                             p_per_information16            in varchar2 default null,
                             p_per_information17            in varchar2 default null,
                             p_per_information18            in varchar2 default null,
                             p_per_information19            in varchar2 default null,
                             p_per_information20            in varchar2 default null,
                             p_per_information21            in varchar2 default null,
                             p_per_information22            in varchar2 default null,
                             p_per_information23            in varchar2 default null,
                             p_per_information24            in varchar2 default null,
                             p_per_information25            in varchar2 default null,
                             p_per_information26            in varchar2 default null,
                             p_per_information27            in varchar2 default null,
                             p_per_information28            in varchar2 default null,
                             p_per_information29            in varchar2 default null,
                             p_per_information30            in varchar2 default null,
                             p_date_of_death                in date default null,
                             p_background_check_status      in varchar2 default null,
                             p_background_date_check        in date default null,
                             p_blood_type                   in varchar2 default null,
                             p_correspondence_language      in varchar2 default null,
                             p_fast_path_employee           in varchar2 default null,
                             p_fte_capacity                 in number default null,
                             p_honors                       in varchar2 default null,
                             p_internal_location            in varchar2 default null,
                             p_last_medical_test_by         in varchar2 default null,
                             p_last_medical_test_date       in date default null,
                             p_mailstop                     in varchar2 default null,
                             p_office_number                in varchar2 default null,
                             p_on_military_service          in varchar2 default null,
                             p_pre_name_adjunct             in varchar2 default null,
                             p_rehire_recommendation        in varchar2 default null,
                             p_projected_start_date         in date default null,
                             p_resume_exists                in varchar2 default null,
                             p_resume_last_updated          in date default null,
                             p_second_passport_exists       in varchar2 default null,
                             p_student_status               in varchar2 default null,
                             p_work_schedule                in varchar2 default null,
                             p_suffix                       in varchar2 default null,
                             p_benefit_group_id             in number default null,
                             p_receipt_of_death_cert_date   in date default null,
                             p_coord_ben_med_pln_no         in varchar2 default null,
                             p_coord_ben_no_cvg_flag        in varchar2 default 'N',
                             p_coord_ben_med_ext_er         in varchar2 default null,
                             p_coord_ben_med_pl_name        in varchar2 default null,
                             p_coord_ben_med_insr_crr_name  in varchar2 default null,
                             p_coord_ben_med_insr_crr_ident in varchar2 default null,
                             p_coord_ben_med_cvg_strt_dt    in date default null,
                             p_coord_ben_med_cvg_end_dt     in date default null,
                             p_uses_tobacco_flag            in varchar2 default null,
                             p_dpdnt_adoption_date          in date default null,
                             p_dpdnt_vlntry_svce_flag       in varchar2 default 'N',
                             p_original_date_of_hire        in date default null,
                             p_adjusted_svc_date            in date default null,
                             p_town_of_birth                in varchar2 default null,
                             p_region_of_birth              in varchar2 default null,
                             p_global_person_id             in varchar2 default null,
                             p_party_id                     in number default null) is
    lv_country    number;
    lv_title      number;
    lv_mar_status number;
    lv_gender     number;
    --lv_business_group                number;
    lv_person_type        number;
    lv_nationality        number;
    lv_country_of_birth   varchar2(50);
    lv_nationality_lookup varchar2(50);
    lv_gender_lookup      varchar2(30);
    lv_title_lookup       varchar2(20);
    lv_marital_status     varchar2(50);
    lv_employee_number    per_people_f.employee_number%TYPE;
    --lv_business_group_id             per_all_people_f.business_group_id%TYPE;
    lv_person_type_id            per_all_people_f.person_type_id%TYPE;
    lv_national_ident            number;
    lv_national_identifier       varchar2(100);
    lx_person_id                 number;
    lx_assignment_id             number;
    lx_per_object_version_number number;
    lx_asg_object_version_number number;
    lx_per_effective_start_date  date;
    lx_per_effective_end_date    date;
    lx_full_name                 varchar2(240);
    lx_per_comment_id            number;
    lx_assignment_sequence       number;
    lx_assignment_number         varchar2(30);
    lx_name_combination_warning  boolean;
    lx_assign_payroll_warning    boolean;
    lx_orig_hire_warning         boolean;
    lv_attribute1                varchar2(240);
    lv_attribute2                varchar2(240);
    lv_attribute5                varchar2(240);
    V_BLOOD_GROUP                varchar2(240);
    lv_error_msg                 varchar2(400);
    status                       varchar2(1) := 'Y';
    -- my_exception exception;
    -- < This part of the code should be added >------------------------
    LV_MSGDATA       varchar2(32000);
    LV_MSGNAME       varchar2(30);
    LV_MSGAPP        varchar2(50);
    LV_MSGENCODED    varchar2(32100);
    LV_MSGENCODEDLEN number(6);
    LV_MSGNAMELOC    number(6);
    LV_MSGTEXTLOC    number(6);
    --------------------------------------------------------------------
  BEGIN
    IF p_hire_date is not null AND p_business_group_id is not null AND
       p_last_name is not null AND p_employee_number is not null and
       p_sex is not null and p_date_of_birth is not null and
       p_date_of_birth < sysdate and p_date_of_birth < p_hire_date and
       trim(upper(p_sex)) in ('MALE', 'FEMALE') THEN
      lv_error_msg := 'Error : ';
      /* Checking if employee already created */
      SELECT COUNT(p.EMPLOYEE_NUMBER)
        INTO lv_employee_number
        FROM PER_ALl_PEOPLE_F p
       WHERE trim(p.EMPLOYEE_NUMBER) = trim(p_employee_number);
      if lv_employee_number > 0 then
        lv_error_msg := lv_error_msg || ' Employee Already Exist. ';
        status       := 'N';
      end if;
      /* checking BUSINESS_GROUP_ID */
      /*
      select count(h.organization_id)
      into   lv_business_group
      from   hr_all_organization_units h
      where  upper(h.name) = upper(p_business_group_name);
      if lv_business_group > 0 then
        select organization_id
        into   lv_business_group_id                   -- lv_business_group_id *
        from   hr_all_organization_units
        where  upper(name) = upper(p_business_group_name);
      end if;
      */
      /* Checking for Valid Person Type */
      select count(ppt.person_type_id)
        into lv_person_type
        from per_person_types ppt
       where upper(trim(ppt.user_person_type)) =
             upper(trim(p_person_type_name))
         and ppt.BUSINESS_GROUP_ID = p_business_group_id;
      if lv_person_type > 0 then
        select ppt.person_type_id
          into lv_person_type_id -- lv_person_type_id
          from per_person_types ppt
         where upper(trim(ppt.user_person_type)) =
               trim(upper(p_person_type_name))
           and ppt.BUSINESS_GROUP_ID = p_business_group_id;
      else
        lv_error_msg   := lv_error_msg || ' Invalid Person Type. ';
        lv_person_type := null;
        status         := 'N';
      end if;
      /* Checking Nationality Lookup */
      if p_nationality_name is not null then
        select count(lookup_code)
          into lv_nationality
          from hr_lookups hl
         where hl.lookup_type = 'NATIONALITY'
           AND upper(trim(HL.meaning)) = trim(upper(p_nationality_name));
        if lv_nationality > 0 then
          select lookup_code
            into lv_nationality_lookup -- lv_nationality_lookup
            from hr_lookups
           where lookup_type = 'NATIONALITY'
             and trim(upper(meaning)) = upper(trim(p_nationality_name));
        else
          lv_error_msg := lv_error_msg || ' Invalid Nationality. ';
          status       := 'N';
        end if;
      end if;
      /* Checking for Gender Lookup */
      if p_sex is not null then
        select count(hl.lookup_code)
          into lv_gender
          from hr_lookups hl
         where hl.lookup_type = 'SEX'
           AND trim(upper(HL.meaning)) = trim(upper(p_sex));
        if lv_gender > 0 then
          select lookup_code
            into lv_gender_lookup -- lv_gender_lookup *
            from hr_lookups
           where lookup_type = 'SEX'
             and trim(upper(meaning)) = trim(upper(p_sex));
        else
          lv_error_msg := lv_error_msg || ' Invalid Gender. ';
          status       := 'N';
        end if;
      end if;
      /* Checking for Marital Status Lookup */
      if p_marital_status is not null then
        select count(hl.lookup_code)
          into lv_mar_status
          from hr_lookups hl
         where hl.lookup_type = 'MAR_STATUS'
           AND trim(upper(HL.meaning)) = trim(upper(p_marital_status));
        if lv_mar_status > 0 then
          select lookup_code
            into lv_marital_status -- lv_marital_status
            from hr_lookups
           where lookup_type = 'MAR_STATUS'
             and trim(upper(meaning)) = trim(upper(p_marital_status));
        else
          lv_error_msg := lv_error_msg || ' Invalid Marital Status. ';
          status       := 'N';
          --lv_mar_status := null;
        end if;
      end if;
      /* Checking for Title Lookup */
      if p_title is not null then
        select count(hl.lookup_code)
          into lv_title
          from hr_lookups hl
         where hl.lookup_type = 'TITLE'
           AND trim(upper(HL.meaning)) = trim(upper(p_title));
        if lv_title > 0 then
          select lookup_code
            into lv_title_lookup -- lv_title_lookup
            from hr_lookups
           where lookup_type = 'TITLE'
             and trim(upper(meaning)) = trim(upper(p_title));
        else
          lv_error_msg := lv_error_msg || ' Invalid Title. ';
          --lv_title_lookup := null;
          status := 'N';
        end if;
      end if;
      /* Checking for Country Lookup */
      if p_country_of_birth is not null then
        select count(hl.lookup_code)
          into lv_country
          from hr_lookups hl
         where hl.lookup_type = 'PER_US_COUNTRY_CODE'
           and upper(trim(hl.meaning)) = upper(trim(p_country_of_birth));
        if lv_country > 0 then
          select lookup_code
            into lv_country_of_birth -- lv_country_of_birth
            from hr_lookups
           where lookup_type = 'PER_US_COUNTRY_CODE'
             and trim(upper(meaning)) = trim(upper(p_country_of_birth));
        else
          lv_error_msg := lv_error_msg || ' Invalid Country of Birth. ';
          --lv_country_of_birth := null;
          status := 'N';
        end if;
      end if;
      -- Checking for Religion Attribute --
      IF p_attribute2 is not null then
        begin
          select fvl.LOOKUP_CODE
            INTO lv_attribute2
            from FND_LOOKUP_VALUES_VL fvl
           where fvl.LOOKUP_TYPE = 'HBL_RELIGION_HR'
             and trim(upper(meaning)) like trim(upper(p_attribute2));
        exception
          when others then
            lv_error_msg := lv_error_msg || ' Invalid Regligion . ';
            --lv_attribute1 := null;
            status := 'N';
        end;
      end if;
      --- checking for Mother Toungue.
      if p_attribute5 is not null then
        begin
          select fvl.FLEX_VALUE_MEANING
            into lv_attribute5
            from FND_FLEX_VALUE_SETS t, FND_FLEX_VALUES_VL fvl
           where t.FLEX_VALUE_SET_ID = fvl.FLEX_VALUE_SET_ID
             and t.FLEX_VALUE_SET_NAME = 'MOTHER_TONGUE_VS'
             and trim(upper(fvl.FLEX_VALUE_MEANING)) like
                 trim(upper(p_attribute5));
        exception
          when others then
            lv_error_msg := lv_error_msg || ' Invalid Mother Tounge . ';
            status       := 'N';
        end;
      end if;
      -- Checking for Blood Group --
      if p_blood_type is not null then
        Begin
          SELECT DISTINCT LV.LOOKUP_CODE
            INTO V_BLOOD_GROUP
            FROM FND_LOOKUP_VALUES LV
           WHERE LV.LOOKUP_TYPE = 'BLOOD_TYPE'
             AND trim(UPPER(LV.MEANING)) = trim(UPPER(p_blood_type));
        Exception
          When NO_DATA_FOUND THEN
            STATUS       := 'N';
            lv_error_msg := lv_error_msg || ' Blood Group Not Found.';
            -- dbms_output.put_line('Blood Group Not Found.');
        END;
      end if;
      if p_national_identifier is not null then
        select count(person_id)
          into lv_national_ident
          from per_all_people_f
         where trim(national_identifier) = trim(p_national_identifier);
        if lv_national_ident > 0 then
          lv_error_msg := lv_error_msg ||
                          ' National Identifier already exist. ';
          --lv_national_identifier := null;
          status := 'N';
        else
          lv_national_identifier := trim(p_national_identifier);
        end if;
      end if;
      if (lv_gender_lookup like 'M%' and
         upper(lv_title_lookup) in ('MRS.', 'MS.', 'MISS')) OR
         (lv_gender_lookup like 'F%' and upper(lv_title_lookup) in ('MR.')) then
        lv_error_msg := lv_error_msg || ' Wrong Title Or Gender . ';
        status       := 'N';
      end if;
      if status <> 'N' then
        --        begin
        /* Calling the create_employee api */
        hr_employee_api.create_employee(p_validate                     => p_validate,
                                        p_hire_date                    => p_hire_date,
                                        p_business_group_id            => p_business_group_id,
                                        p_last_name                    => p_last_name,
                                        p_sex                          => lv_gender_lookup,
                                        p_person_type_id               => lv_person_type_id,
                                        p_per_comments                 => p_per_comments,
                                        p_date_employee_data_verified  => p_date_employee_data_verified,
                                        p_date_of_birth                => p_date_of_birth,
                                        p_email_address                => p_email_address,
                                        p_employee_number              => p_employee_number,
                                        p_expense_check_send_to_addres => p_expense_check_send_to_addres,
                                        p_first_name                   => p_first_name,
                                        p_known_as                     => p_known_as,
                                        p_marital_status               => lv_marital_status,
                                        p_middle_names                 => p_middle_names,
                                        p_nationality                  => lv_nationality_lookup,
                                        p_national_identifier          => lv_national_identifier,
                                        p_previous_last_name           => p_previous_last_name,
                                        p_registered_disabled_flag     => p_registered_disabled_flag,
                                        p_title                        => lv_title_lookup,
                                        p_vendor_id                    => p_vendor_id,
                                        p_work_telephone               => p_work_telephone,
                                        p_attribute_category           => 'Personal Extra Information' /*p_attribute_category*/,
                                        p_attribute1                   => lv_attribute1,
                                        p_attribute2                   => lv_attribute2,
                                        p_attribute3                   => p_attribute3,
                                        p_attribute4                   => p_attribute4,
                                        p_attribute5                   => lv_attribute5,
                                        p_attribute6                   => p_attribute6,
                                        p_attribute7                   => p_attribute7,
                                        p_attribute8                   => p_attribute8,
                                        p_attribute9                   => p_attribute9,
                                        p_attribute10                  => p_attribute10,
                                        p_attribute11                  => p_attribute11,
                                        p_attribute12                  => p_attribute12,
                                        p_attribute13                  => p_attribute13,
                                        p_attribute14                  => p_attribute14,
                                        p_attribute15                  => p_attribute15,
                                        p_attribute16                  => p_attribute16,
                                        p_attribute17                  => p_attribute17,
                                        p_attribute18                  => p_attribute18,
                                        p_attribute19                  => p_attribute19,
                                        p_attribute20                  => p_attribute20,
                                        p_attribute21                  => p_attribute21,
                                        p_attribute22                  => p_attribute22,
                                        p_attribute23                  => p_attribute23,
                                        p_attribute24                  => p_attribute24,
                                        p_attribute25                  => p_attribute25,
                                        p_attribute26                  => p_attribute26,
                                        p_attribute27                  => p_attribute27,
                                        p_attribute28                  => p_attribute28,
                                        p_attribute29                  => p_attribute29,
                                        p_attribute30                  => p_attribute30,
                                        p_per_information_category     => p_per_information_category,
                                        p_per_information1             => p_per_information1,
                                        p_per_information2             => p_per_information2,
                                        p_per_information3             => p_per_information3,
                                        p_per_information4             => p_per_information4,
                                        p_per_information5             => p_per_information5,
                                        p_per_information6             => p_per_information6,
                                        p_per_information7             => p_per_information7,
                                        p_per_information8             => p_per_information8,
                                        p_per_information9             => p_per_information9,
                                        p_per_information10            => p_per_information10,
                                        p_per_information11            => p_per_information11,
                                        p_per_information12            => p_per_information12,
                                        p_per_information13            => p_per_information13,
                                        p_per_information14            => p_per_information14,
                                        p_per_information15            => p_per_information15,
                                        p_per_information16            => p_per_information16,
                                        p_per_information17            => p_per_information17,
                                        p_per_information18            => p_per_information18,
                                        p_per_information19            => p_per_information19,
                                        p_per_information20            => p_per_information20,
                                        p_per_information21            => p_per_information21,
                                        p_per_information22            => p_per_information22,
                                        p_per_information23            => p_per_information23,
                                        p_per_information24            => p_per_information24,
                                        p_per_information25            => p_per_information25,
                                        p_per_information26            => p_per_information26,
                                        p_per_information27            => p_per_information27,
                                        p_per_information28            => p_per_information28,
                                        p_per_information29            => p_per_information29,
                                        p_per_information30            => p_per_information30,
                                        p_date_of_death                => p_date_of_death,
                                        p_background_check_status      => p_background_check_status,
                                        p_background_date_check        => p_background_date_check,
                                        p_blood_type                   => V_BLOOD_GROUP /*p_blood_type*/,
                                        p_correspondence_language      => p_correspondence_language,
                                        p_fast_path_employee           => p_fast_path_employee,
                                        p_fte_capacity                 => p_fte_capacity,
                                        p_honors                       => p_honors,
                                        p_internal_location            => p_internal_location,
                                        p_last_medical_test_by         => p_last_medical_test_by,
                                        p_last_medical_test_date       => p_last_medical_test_date,
                                        p_mailstop                     => p_mailstop,
                                        p_office_number                => p_office_number,
                                        p_on_military_service          => p_on_military_service,
                                        p_pre_name_adjunct             => p_pre_name_adjunct,
                                        p_rehire_recommendation        => p_rehire_recommendation,
                                        p_projected_start_date         => p_projected_start_date,
                                        p_resume_exists                => p_resume_exists,
                                        p_resume_last_updated          => p_resume_last_updated,
                                        p_second_passport_exists       => p_second_passport_exists,
                                        p_student_status               => p_student_status,
                                        p_work_schedule                => p_work_schedule,
                                        p_suffix                       => p_suffix,
                                        p_benefit_group_id             => p_benefit_group_id,
                                        p_receipt_of_death_cert_date   => p_receipt_of_death_cert_date,
                                        p_coord_ben_med_pln_no         => p_coord_ben_med_pln_no,
                                        p_coord_ben_no_cvg_flag        => p_coord_ben_no_cvg_flag,
                                        p_coord_ben_med_ext_er         => p_coord_ben_med_ext_er,
                                        p_coord_ben_med_pl_name        => p_coord_ben_med_pl_name,
                                        p_coord_ben_med_insr_crr_name  => p_coord_ben_med_insr_crr_name,
                                        p_coord_ben_med_insr_crr_ident => p_coord_ben_med_insr_crr_ident,
                                        p_coord_ben_med_cvg_strt_dt    => p_coord_ben_med_cvg_strt_dt,
                                        p_coord_ben_med_cvg_end_dt     => p_coord_ben_med_cvg_end_dt,
                                        p_uses_tobacco_flag            => p_uses_tobacco_flag,
                                        p_dpdnt_adoption_date          => p_dpdnt_adoption_date,
                                        p_dpdnt_vlntry_svce_flag       => p_dpdnt_vlntry_svce_flag,
                                        p_original_date_of_hire        => p_original_date_of_hire,
                                        p_adjusted_svc_date            => p_adjusted_svc_date,
                                        p_town_of_birth                => p_town_of_birth,
                                        p_region_of_birth              => p_region_of_birth,
                                        p_country_of_birth             => lv_country_of_birth,
                                        p_global_person_id             => p_global_person_id,
                                        p_party_id                     => p_party_id,
                                        p_person_id                    => lx_person_id,
                                        p_assignment_id                => lx_assignment_id,
                                        p_per_object_version_number    => lx_per_object_version_number,
                                        p_asg_object_version_number    => lx_asg_object_version_number,
                                        p_per_effective_start_date     => lx_per_effective_start_date,
                                        p_per_effective_end_date       => lx_per_effective_end_date,
                                        p_full_name                    => lx_full_name,
                                        p_per_comment_id               => lx_per_comment_id,
                                        p_assignment_sequence          => lx_assignment_sequence,
                                        p_assignment_number            => lx_assignment_number,
                                        p_name_combination_warning     => lx_name_combination_warning,
                                        p_assign_payroll_warning       => lx_assign_payroll_warning,
                                        p_orig_hire_warning            => lx_orig_hire_warning);
        --   DBMS_OUTPUT.put_line('Employee(s) Successfully Created');
        --   DBMS_OUTPUT.put_line('Employee No : ' || p_employee_number);
        p_employee_number := null;
        --< This part of the code should be added >-------------------------
        LV_MSGENCODED    := fnd_message.get_encoded();
        LV_MSGENCODEDLEN := LENGTH(LV_MSGENCODED);
        LV_MSGNAMELOC    := INSTR(LV_MSGENCODED, chr(0));
        LV_MSGAPP        := SUBSTR(LV_MSGENCODED, 1, LV_MSGNAMELOC - 1);
        LV_MSGENCODED    := SUBSTR(LV_MSGENCODED,
                                   LV_MSGNAMELOC + 1,
                                   LV_MSGENCODEDLEN);
        LV_MSGENCODEDLEN := LENGTH(LV_MSGENCODED);
        LV_MSGTEXTLOC    := INSTR(LV_MSGENCODED, chr(0));
        LV_MSGNAME       := SUBSTR(LV_MSGENCODED, 1, LV_MSGTEXTLOC - 1);
        if (LV_MSGNAME <> 'CONC-SINGLE PENDING REQUEST') then
          fnd_message.set_name(LV_MSGAPP, LV_MSGNAME);
        end if;
      else
        raise_application_error(-20000, lv_error_msg);
        --       exception
        --         when others then
        --            dbms_output.put_line(' ERROR : '||SQLERRM);
        ----         end;
        --             else
        --       dbms_output.put_line(lv_error_msg);
      end if;
    ELSE
      --DBMS_OUTPUT.put_line('ERROR in Creating Employee : Check Parameters : Emp No. , Hire_date, Business Group, Gender, Last Name, Date of Birth');
      lv_error_msg := 'ERROR in Creating Employee : Check Parameters : Emp No. , Hire_date, Business Group, Gender, Last Name, Date of Birth';
      raise_application_error(-20000, lv_error_msg);
    END IF;
  exception
    when others then
      lv_error_msg := sqlerrm;
      raise_application_error(-20000, lv_error_msg);
  END IBT_EMPLOYEE_API;

  procedure update_emp_salary(p_empno in varchar2) is
    lb_inv_next_sal_date_warning BOOLEAN;
    lb_proposed_salary_warning   BOOLEAN;
    lb_approved_warning          BOOLEAN;
    lb_payroll_warning           BOOLEAN;
    ln_pay_proposal_id           NUMBER := 15810;
    ln_object_version_number     NUMBER := 2;
    lv_error_msg                 varchar2(200);
  
    cursor c1 is
      select *
        from hbl_integ_stage.HBL_EMP_SALARY_UPDATE
       where employee_number = nvl(p_empno, employee_number)
         and nvl(status, 'N') != 'S';
  BEGIN
    execute immediate 'ALTER SESSION SET NLS_LANGUAGE= ''AMERICAN''';
    -- Create or Upadte Employee Salary Proposal
    -- ----------------------------------------------------------------
    for i in c1 loop
      begin
        lv_error_msg := NULL;
        select ppp.PAY_PROPOSAL_ID, ppp.OBJECT_VERSION_NUMBER
          into ln_pay_proposal_id, ln_object_version_number
          from per_people_x          pap,
               per_all_assignments_f paa,
               per_pay_proposals     ppp
         where pap.EMPLOYEE_NUMBER = i.employee_number
           and pap.PERSON_ID = paa.PERSON_ID
           and paa.EFFECTIVE_END_DATE = apps.hr_general.END_OF_TIME
           and paa.ASSIGNMENT_ID = ppp.ASSIGNMENT_ID
           and ppp.CHANGE_DATE = i.change_date;
      
        apps.hr_maintain_proposal_api.update_salary_proposal(p_pay_proposal_id => ln_pay_proposal_id,
                                                             --  p_business_group_id => 81,
                                                             --p_assignment_id     => 12794,
                                                             -- p_change_date       => TO_DATE('01-JAN-2018'),
                                                             p_date_to           => null,
                                                             p_proposed_salary_n => i.new_basic,
                                                             p_approved          => 'Y',
                                                             -- Output data elements
                                                             -- --------------------------------
                                                             --p_pay_proposal_id           => ln_pay_proposal_id,
                                                             p_object_version_number     => ln_object_version_number,
                                                             p_inv_next_sal_date_warning => lb_inv_next_sal_date_warning,
                                                             p_proposed_salary_warning   => lb_proposed_salary_warning,
                                                             p_approved_warning          => lb_approved_warning,
                                                             p_payroll_warning           => lb_payroll_warning);
      
        update hbl_integ_stage.HBL_EMP_SALARY_UPDATE
           set status = 'S', erorr_desc = lv_error_msg
         where employee_number = i.employee_number;
        COMMIT;
      exception
        WHEN OTHERS THEN
          ROLLBACK;
          lv_error_msg := substr(sqlerrm, 200);
          update hbl_integ_stage.HBL_EMP_SALARY_UPDATE
             set status = 'E', erorr_desc = lv_error_msg
           where employee_number = i.employee_number;
          dbms_output.put_line(SQLERRM);
      end;
    end loop;
  END update_emp_salary;

END Hbl_hr_pkg;
/
