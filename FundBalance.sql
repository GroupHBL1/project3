SELECT ev.employee_number,
       ev.full_name,
       to_date(sysdate ) As_On,
       APPS.XXFUND_PKG.get_fund_bal(p_fund_id       => 2,
                                    p_group         => 'PFEE',
                                    p_trans_type_id => null,
                                    p_person_id     => ev.PERSON_ID,
                                    p_date          => to_date(sysdate)) PF_EE_BAL,
       APPS.XXFUND_PKG.get_fund_bal(p_fund_id       => 2,
                                    p_group         => 'PFER',
                                    p_trans_type_id => null,
                                    p_person_id     => ev.PERSON_ID,
                                    p_date          => to_date(sysdate )) PF_ER_BAL,

       (select abs(sum(t.Amount)) bal_amount
          from xxfund_transactions t, xxfund_tran_type_setup S
         where t.trans_type_id = s.tran_type_id
           and trans_date <= to_date(sysdate ) --to_date(:$FLEX$.As_on_Date, 'YYYY/MM/DD HH24:MI:SS')
           and fund_group in ('PFEE')
           and trans_class in ('WD', 'WDR')
           AND T.PERSON_ID = EV.PERSON_ID having sum(t.Amount) < 0) W_BAL
  FROM HBL_EMP_ASSIG_V eV
 WHERE 1 =1 
 --and eV.ASSIGNMENT_STATUS_TYPE_ID = 1
   AND eV.BUSINESS_GROUP_ID = 81
   and ev.employee_number in 
   (select  * from xxhbl_temp tt)
